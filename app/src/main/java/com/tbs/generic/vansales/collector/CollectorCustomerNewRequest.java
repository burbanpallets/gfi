package com.tbs.generic.vansales.collector;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.generic.vansales.Model.CustomerDo;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ProgressDialogTask;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class CollectorCustomerNewRequest extends AsyncTask<String, Void, Boolean> {

    ArrayList<CustomerDo> customers;

    private Context mContext;
    private String id;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;

    public CollectorCustomerNewRequest(Context mContext) {

        this.mContext = mContext;
    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        void onCompleted(boolean isError, ArrayList<CustomerDo> customers);

    }

    public boolean runRequest() {
        // System.out.println("CUSTOMER ID " + customer);
        String NAMESPACE = "http://www.adonix.com/WSS";
        String METHOD_NAME = "run";
        String SOAP_ACTION = "CAdxWebServiceXmlCC";
        preferenceUtils = new PreferenceUtils(mContext);
        username = preferenceUtils.getStringFromPreference(PreferenceUtils.A_USER_NAME, "");
        password = preferenceUtils.getStringFromPreference(PreferenceUtils.A_PASSWORD, "");
        ip = preferenceUtils.getStringFromPreference(PreferenceUtils.IP_ADDRESS, "");
        port = preferenceUtils.getStringFromPreference(PreferenceUtils.PORT, "");
        pool = preferenceUtils.getStringFromPreference(PreferenceUtils.ALIAS, "");
        String URL = "http://" + ip + ":" + port + "/soap-generic/syracuse/collaboration/syracuse/CAdxWebServiceXmlCC";
        String id = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "");
        int role = preferenceUtils.getIntFromPreference(PreferenceUtils.USER_ROLE, 0);

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        // Set all input params
        request.addProperty("publicName", WebServiceConstants.CUSTOMER_COLLECOR);
        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("I_YBPC", id);
            jsonObject.put("I_YAPUSR", role);

        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        request.addProperty("inputXml", jsonObject.toString());

        SoapObject callcontext = new SoapObject("", "callContext");
        // Set all input params
        callcontext.addProperty("codeLang", "ENG");
        callcontext.addProperty("poolAlias", pool);
        callcontext.addProperty("poolId", "");
        callcontext.addProperty("codeUser", username);
        callcontext.addProperty("password", password);
        callcontext.addProperty("requestConfig", "adxwss.trace.on=off");

        request.addSoapObject(callcontext);


        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

        envelope.setOutputSoapObject(request);

        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
        androidHttpTransport.debug = true;

        try {
            List<HeaderProperty> headerList = new ArrayList<HeaderProperty>();
            headerList.add(new HeaderProperty("Authorization", "Basic " + org.kobjects.base64.Base64.encode((username + ":" + password).getBytes())));


            androidHttpTransport.call(SOAP_ACTION, envelope, headerList);


            SoapObject response = (SoapObject) envelope.getResponse();
            String resultXML = (String) response.getProperty("resultXml");
            if (resultXML != null && resultXML.length() > 0) {
                return parseXML(resultXML);
            } else {
                return false;
            }
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();
            CustomerDo customer = null;
            customers = new ArrayList<>();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

//
                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        customer = new CustomerDo();
                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();
                    if (startTag != null && startTag.equalsIgnoreCase("FLD")) {
                        if (attribute.equalsIgnoreCase("O_YBPCNUM")) {
                            if (text.length() > 0) {
                                customer.customer = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YBPCSHO")) {
                            if (text.length() > 0) {

                                customer.customerName = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YBPADDLIG")) {
                            if (text.length() > 0) {

                                customer.postBox = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YBPADDLIG1")) {
                            if (text.length() > 0) {

                                customer.landmark = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YBPADDLIG2")) {
                            if (text.length() > 0) {

                                customer.town = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YCRYNAME")) {
                            if (text.length() > 0) {

                                customer.countryName = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YCITY")) {
                            if (text.length() > 0) {

                                customer.city = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YWEB")) {
                            if (text.length() > 0) {

                                customer.webSite = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YPOS")) {
                            if (text.length() > 0) {

                                customer.postalCode = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YLD")) {
                            if (text.length() > 0) {

                                customer.landline = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YEM")) {
                            if (text.length() > 0) {

                                customer.email = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YFAX")) {
                            if (text.length() > 0) {

                                customer.fax = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YMOB")) {
                            if (text.length() > 0) {

                                customer.mobile = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YLAT")) {
                            if (text.length() > 0) {

                                customer.lattitude = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_LON")) {
                            if (text.length() > 0) {

                                customer.longitude = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YBPCRAMT")) {
                            if (text.length() > 0) {

                                customer.amount = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YBUS")) {
                            if (text.length() > 0) {

                                customer.businessLine = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YSAT")) {
                            if (text.length() > 0) {

                                customer.emirates = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YBPFLG")) {
                            if (text.length() > 0) {

                                customer.flag = Integer.parseInt(text);
//                                if(customer.flag==0){
//                                    customer.isCreditAvailable=true;
//                                }else {
//                                    customer.isCreditAvailable=false;
//
//                                }
                            }
                        }
                        text = "";


                    }
                    if (endTag.equalsIgnoreCase("LIN")) {
                        customers.add(customer);
                    }
                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();


                }
                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception " + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        ProgressDialogTask.getInstance().showProgress(mContext, false, "");
        //((BaseActivity) mContext).showLoader();

//         ProgressTask.getInstance().showProgress(mContext, false, "Loading...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        ProgressDialogTask.getInstance().closeProgress();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, customers);
        }
    }
}