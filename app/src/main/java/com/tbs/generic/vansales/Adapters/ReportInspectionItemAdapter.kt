package com.tbs.rentalmanagement.inspection.adapters

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.tbs.generic.vansales.Model.ReportQuestionsDO
import com.tbs.generic.vansales.Model.SerialListDO
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.utils.CalendarUtils

import kotlinx.android.synthetic.main.item_inspection_report.view.*
import java.util.*


class ReportInspectionItemAdapter(
    private val context: Context
    , internal var productList: ArrayList<ReportQuestionsDO>
) :
    RecyclerView.Adapter<ReportInspectionItemAdapter.MyViewHolder>() {

    private val selectedLoanReturnDOs = java.util.ArrayList<SerialListDO>()

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvItemInspection: TextView = view.tv_item_inspection
        val tvInspectionResponse: CheckBox = view.tv_item_response
        val tvInspectionComment: TextView = view.tv_item_comment

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_inspection_report, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var questionsDO = productList!!.get(position)
        holder.tvItemInspection.setText("" + questionsDO.inspectionItem)
        if (questionsDO.reponse.equals("1")) {
            holder.tvInspectionResponse.isChecked = false

        } else {
            holder.tvInspectionResponse.isChecked = true
        }
        holder.tvInspectionComment.setText("" +  CalendarUtils.getStringOfHexadecilmal(questionsDO.comment))


    }

    override fun getItemCount(): Int {
        return productList!!.size
    }


}
