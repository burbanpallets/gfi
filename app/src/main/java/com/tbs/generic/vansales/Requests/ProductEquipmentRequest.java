package com.tbs.generic.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.PurchaseReceiptDO;
import com.tbs.generic.vansales.Model.TrailerSelectionDO;
import com.tbs.generic.vansales.Model.TrailerSelectionMainDO;
import com.tbs.generic.vansales.Model.UserDeliveryDO;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.util.ArrayList;

public class ProductEquipmentRequest extends AsyncTask<String, Void, Boolean> {

    private TrailerSelectionMainDO trailerSelectionMainDO;
    private TrailerSelectionDO trailerSelectionDO;
    private Context mContext;
    PreferenceUtils preferenceUtils;


    public ProductEquipmentRequest(Context mContext) {

        this.mContext = mContext;


    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        void onCompleted(boolean isError, TrailerSelectionMainDO trailerSelectionMainDO);

    }

    public boolean runRequest() {
        preferenceUtils = new PreferenceUtils(mContext);
        String id = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "");
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("I_YVEHROU", id);
        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        WebServiceAcess webServiceAcess = new WebServiceAcess();
        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.PRODUCT_SELECTION, jsonObject);
        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("VR selection xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            trailerSelectionMainDO = new TrailerSelectionMainDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    attribute = xpp.getAttributeValue(null, "ID");
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("TAB") && attribute.equalsIgnoreCase("GRP2")) {
                        preferenceUtils.saveString(PreferenceUtils.GRP, "GRP2");
                        trailerSelectionMainDO.trailerSelectionDOS = new ArrayList<>();
                    } else if (startTag.equalsIgnoreCase("TAB") && attribute.equalsIgnoreCase("GRP3")) {
                        trailerSelectionMainDO.equipmentDOS = new ArrayList<>();
                        preferenceUtils.saveString(PreferenceUtils.GRP, "GRP3");
                    } else if (startTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP2")) {
                        trailerSelectionDO = new TrailerSelectionDO();
                    } else if (startTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").equalsIgnoreCase("GRP3")) {
                        trailerSelectionDO = new TrailerSelectionDO();
                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {
                        if (attribute.equalsIgnoreCase("O_YEQPCODE")) {
                            trailerSelectionDO.trailer = text;


                        } else if (attribute.equalsIgnoreCase("O_YEQPDES")) {
                            if (text.length() > 0) {

                                trailerSelectionDO.trailerDescription = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YSDLNUM")) {
                            if (text.length() > 0) {

                                trailerSelectionDO.loandelivery = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YSDDLIN")) {
                            if (text.length() > 0) {

                                trailerSelectionDO.linenumber = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YSTOFCY")) {
                            if (text.length() > 0) {

                                trailerSelectionDO.stocksite = text;
                            }

                        }else if (attribute.equalsIgnoreCase("O_YPONO")) {
                            if (text.length() > 0) {

                                trailerSelectionDO.poNumber = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YBPCODE")) {
                            if (text.length() > 0) {

                                trailerSelectionDO.customer = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YBPNAME")) {
                            if (text.length() > 0) {

                                trailerSelectionDO.customerDescription = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YQTY")) {
                            if (text.length() > 0) {

                                trailerSelectionDO.quantity = Integer.valueOf(text);
                            }
                        }


                        else if (attribute.equalsIgnoreCase("O_YTOTEQPCODE")) {
                            if (text.length() > 0) {

                                trailerSelectionDO.trailer = text;
                            }

                        } else if (attribute.equalsIgnoreCase("O_YTOTEQPDES")) {
                            if (text.length() > 0) {

                                trailerSelectionDO.trailerDescription = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YTOTEQPQTY")) {
                            if (text.length() > 0) {

                                trailerSelectionDO.quantity = Integer.valueOf(text);
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_YITM")) {
                            if (text.length() > 0) {

                                trailerSelectionMainDO.item = text;
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_YFCY")) {
                            if (text.length() > 0) {

                                trailerSelectionMainDO.stockSite = text;
                            }
                        }
                        else if (attribute.equalsIgnoreCase("O_YITMDES")) {
                            if (text.length() > 0) {

                                trailerSelectionMainDO.itemDes = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_YTOTQTY")) {
                            if (text.length() > 0) {

                                trailerSelectionMainDO.totalqty = Integer.valueOf(text);
                            }
                        }
                        text = "";
                    }


                     if (endTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "").
                             equalsIgnoreCase("GRP2")) {
                         trailerSelectionMainDO.trailerSelectionDOS.add(trailerSelectionDO);
                    }
                     else if (endTag.equalsIgnoreCase("LIN") && preferenceUtils.getStringFromPreference(PreferenceUtils.GRP, "")
                             .equalsIgnoreCase("GRP3")) {
                         trailerSelectionMainDO.equipmentDOS.add(trailerSelectionDO);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    if (xpp.getText().length() > 0) {

                        text = xpp.getText();
                    } else {
                        text = "";
                    }
                }


                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        ((BaseActivity) mContext).showLoader();
        // ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        //  ProgressTask.getInstance().closeProgress();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, trailerSelectionMainDO);
        }
        ((BaseActivity) mContext).hideLoader();

    }
}