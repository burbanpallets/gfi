package com.tbs.generic.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.DriverDO;
import com.tbs.generic.vansales.Model.DriverEmptyDO;
import com.tbs.generic.vansales.Model.DriverIdMainDO;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.utils.AppPrefs;
import com.tbs.generic.vansales.utils.Constants;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class NonScheduledDriverIdRequest extends AsyncTask<String, Void, Boolean> {

    private DriverIdMainDO driverIdMainDO;
    private DriverDO driverDO;
    private Context mContext;
    private String id;
    PreferenceUtils preferenceUtils;

    public NonScheduledDriverIdRequest(String id, Context mContext) {

        this.mContext = mContext;
        this.id = id;
    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        void onCompleted(boolean isError, DriverIdMainDO driverIdMainDO);

    }

    public boolean runRequest() {
        preferenceUtils = new PreferenceUtils(mContext);

        id = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "");
        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("I_YDRIVER", id);
            jsonObject.put("I_YSLMEN", "456");

        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        WebServiceAcess webServiceAcess = new WebServiceAcess();
        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.NON_SCHEDULED_ROUTE_ID, jsonObject);
        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }
    }

    public boolean parseXML(String xmlString) {
        System.out.println("Non Scheduled xmlString " + xmlString);
        try {
            String text = "", attribute = "", attribute1 = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            driverIdMainDO = new DriverIdMainDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        attribute1 = xpp.getAttributeValue(null, "ID");

                        driverIdMainDO.driverDOS = new ArrayList<>();



                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        // String  attribute2 = xpp.getAttributeValue(null, "ID");
                        driverDO = new DriverDO();

                    }

                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {


                        if (attribute.equalsIgnoreCase("O_YDRIVER")) {
                            driverDO.driver = text;


                        }
                      else   if (attribute.equalsIgnoreCase("O_YDNAME")) {
                            driverDO.driverName = text;


                        }
                        else if (attribute.equalsIgnoreCase("O_VEHCODE")) {
                            driverDO.vehicleCode = text;


                        }
                       else if (attribute.equalsIgnoreCase("O_YLOC")) {
                            if(text.length()>0){
                                driverDO.location = text;

                            }


                        }
                        else if (attribute.equalsIgnoreCase("O_VEHNO")) {
                            driverDO.plate = text;

                        } else if (attribute.equalsIgnoreCase("O_YUNROU")) {
                            driverDO.vehicleRouteId = text;

                        } else if (attribute.equalsIgnoreCase("O_YFCY")) {
                            driverDO.siteId = text;

                        }
                        else if (attribute.equalsIgnoreCase("O_YFCYDES")) {
                            driverDO.site = text;

                        }
                        else if (attribute.equalsIgnoreCase("O_YREF")) {
                            driverDO.reference = text;

                        } else if (attribute.equalsIgnoreCase("O_YTRIP")) {
                            driverDO.tripNumber = text;

                        }
                        else if (attribute.equalsIgnoreCase("O_YCHKIN")) {
                            if (text.length() > 0) {

                                driverDO.checkinFlag = Integer.parseInt(text);
                            }

                        }

                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        driverIdMainDO.driverDOS.add(driverDO);


                    }

                    text="";
                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        //  ((BaseActivity)mContext).showLoader();

    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        if (onResultListener != null) {
            onResultListener.onCompleted(!result, driverIdMainDO);
        }
    }
}