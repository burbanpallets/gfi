package com.tbs.generic.vansales.Activitys

import android.annotation.SuppressLint
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tbs.generic.vansales.Adapters.SpotSalesCustomerAdapter
import com.tbs.generic.vansales.Model.CustomerDo
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.CustomerNewRequest
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.utils.Util
import java.util.*

class SpotSalesCustomerActivity : BaseActivity() {
    lateinit var  userId: String
    lateinit var orderCode: String
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var customerAdapter: SpotSalesCustomerAdapter
    lateinit var llOrderHistory: LinearLayout
    lateinit var customerDos: ArrayList<CustomerDo>
    private lateinit var tvScreenTitles : TextView
    private lateinit var llSearch : LinearLayout
    private lateinit var etSearch : EditText
    private lateinit var ivClearSearch : ImageView
    private lateinit var ivGoBack : ImageView
    private lateinit var ivSearchs : ImageView
    private lateinit var tvNoOrders : TextView




    override fun initialize() {
        llOrderHistory = layoutInflater.inflate(R.layout.site_screen, null) as LinearLayout
        llBody.addView(llOrderHistory, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        flToolbar.visibility = View.GONE
        changeLocale()
        disableMenuWithBackButton()
        initializeControls()
        tvScreenTitles.text = getString(R.string.select_customer)

        tvScreenTitles.visibility = View.VISIBLE
        ivGoBack.setOnClickListener{
            Util.preventTwoClick(it)
            finish()
        }

        ivSearchs.setOnClickListener{
            Util.preventTwoClick(it)
            //            if(llSearch.visibility == View.VISIBLE){
//                llSearch.visibility = View.INVISIBLE
//            }
//            else{
//            }
            tvScreenTitles.visibility= View.GONE
            llSearch.visibility = View.VISIBLE
        }

        ivClearSearch.setOnClickListener{
            Util.preventTwoClick(it)
            etSearch.setText("")
            if(customerDos!=null &&customerDos.size>0){
                customerAdapter = SpotSalesCustomerAdapter(this@SpotSalesCustomerActivity, customerDos,"",0)
                recycleview.adapter = customerAdapter
                tvNoOrders.visibility = View.GONE
                recycleview.visibility = View.VISIBLE
            }
            else{
                tvNoOrders.visibility = View.VISIBLE
                recycleview.visibility = View.GONE
            }
        }

        etSearch.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(editable: Editable?) {
                if(etSearch.text.toString().equals("", true)){
                    if(customerDos!=null &&customerDos.size>0){
                        customerAdapter        =  SpotSalesCustomerAdapter(this@SpotSalesCustomerActivity, customerDos,"",0)
                        recycleview.adapter    = customerAdapter
                        tvNoOrders.visibility  = View.GONE
                        recycleview.visibility = View.VISIBLE
                    }
                    else{
                        tvNoOrders.visibility  = View.VISIBLE
                        recycleview.visibility = View.GONE
                    }
                }
                else if(etSearch.text.toString().length>2){
                    filter(etSearch.text.toString())
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })

        val driverListRequest = CustomerNewRequest(this@SpotSalesCustomerActivity)
        customerDos = StorageManager.getInstance(this).getSpotSalesCustomerList(this)
        if(customerDos!=null &&customerDos.size>0){
            customerAdapter = SpotSalesCustomerAdapter(this@SpotSalesCustomerActivity, customerDos,"",0)
            recycleview.adapter = customerAdapter
            recycleview.visibility = View.VISIBLE
            tvNoOrders.visibility = View.GONE
        }
        else{
            driverListRequest.setOnResultListener { isError, customerDOs ->
                hideLoader()
                if (isError) {
                    Toast.makeText(this@SpotSalesCustomerActivity, R.string.error_customer_list, Toast.LENGTH_SHORT).show()
                } else {
                    customerDos = customerDOs
                    if(customerDos!=null &&customerDos.size>0){
                        StorageManager.getInstance(this).saveSpotSalesCustomerList(this, customerDOs)
                        customerAdapter        = SpotSalesCustomerAdapter(this@SpotSalesCustomerActivity, customerDos,"",0)
                        recycleview.adapter    = customerAdapter
                        recycleview.visibility = View.VISIBLE
                        tvNoOrders.visibility  = View.GONE
                    }
                    else{
                        recycleview.visibility = View.GONE
                        tvNoOrders.visibility  = View.VISIBLE
                        showToast(getString(R.string.no_spot_sales_customer))
                    }
                }
            }
            driverListRequest.execute()
        }
    }

    override fun initializeControls() {
        recycleview          = findViewById<View>(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        llSearch             = findViewById<View>(R.id.llSearch) as LinearLayout
        etSearch             = findViewById<View>(R.id.etSearch) as EditText
        ivClearSearch        = findViewById<View>(R.id.ivClearSearch) as ImageView
        ivGoBack             = findViewById<View>(R.id.ivGoBack) as ImageView
        ivSearchs            = findViewById<View>(R.id.ivSearchs) as ImageView
        tvNoOrders           = findViewById<View>(R.id.tvNoOrders) as TextView
        tvScreenTitles       = findViewById<View>(R.id.tvScreenTitles) as TextView

        recycleview.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this@SpotSalesCustomerActivity, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
    }

    private fun filter(filtered : String) : ArrayList<CustomerDo>{
        val customerDOs = ArrayList<CustomerDo>()
        for (i in customerDos.indices){
            if(customerDos.get(i).customerName.contains(filtered, true)
                    ||customerDos.get(i).customer.contains(filtered, true)){
                customerDOs.add(customerDos.get(i))
            }
        }
        if(customerDOs.size>0){
            customerAdapter.refreshAdapter(customerDOs)
            tvNoOrders.visibility = View.GONE
            recycleview.visibility = View.VISIBLE
        }
        else{
            tvNoOrders.visibility = View.VISIBLE
            recycleview.visibility = View.GONE
        }
        return customerDOs
    }

}
