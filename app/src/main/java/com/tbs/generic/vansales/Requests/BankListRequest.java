package com.tbs.generic.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.BankDO;
import com.tbs.generic.vansales.Model.BankMainDO;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.utils.AppPrefs;
import com.tbs.generic.vansales.utils.Constants;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class BankListRequest extends AsyncTask<String, Void, Boolean> {

    private BankMainDO bankMainDO;
    private BankDO bankDO;
    private Context mContext;
    private String site;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;
    String customer;


    public BankListRequest(String typE, Context mContext) {

        this.mContext = mContext;
        this.site = typE;


    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        void onCompleted(boolean isError, BankMainDO bankMainDO);

    }

    public boolean runRequest() {
        JSONObject jsonObject = new JSONObject();

        try {

//            jsonObject.put("I_YBPC", "ZA002");

            jsonObject.put("I_YFCY", site);

        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        WebServiceAcess webServiceAcess = new WebServiceAcess();

        String resultXML = webServiceAcess.runRequest(mContext,ServiceURLS.runAction, WebServiceConstants.BANK_LIST, jsonObject);

        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }
//        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            bankMainDO = new BankMainDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {
                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        bankMainDO.bankDOS = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        bankDO = new BankDO();

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {
                        if (attribute.equalsIgnoreCase("I_YBPC")) {
                            if(text.length()>0){

                                bankMainDO.site = text;
                            }


                        }
                        else if (attribute.equalsIgnoreCase("O_YBAN")) {
                            if(text.length()>0){

                                bankDO.bankId = text;
                            }

                        }else if (attribute.equalsIgnoreCase("O_YBANDES")) {
                            if(text.length()>0){

                                bankDO.bankName = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YCUR")) {
                            if(text.length()>0){

                                bankDO.currency = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YBANACC")) {
                            if(text.length()>0){

                                bankDO.bankAccount = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YCPY")) {
                            if(text.length()>0){

                                bankDO.company = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YCPYDES")) {
                            if(text.length()>0){

                                bankDO.companyDescription = text;
                            }

                        }
                        else if (attribute.equalsIgnoreCase("O_YABNCRY")) {
                            if(text.length()>0){

                                bankDO.countryId = text;
                            }

                        }

                    }

                    if (endTag.equalsIgnoreCase("GRP")) {
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        bankMainDO.bankDOS.add(bankDO);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    if(xpp.getText().length()>0){

                        text = xpp.getText();
                    }else {
                        text="";
                    }
                }


                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        ((BaseActivity)mContext).showLoader();
       // ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        ((BaseActivity)mContext).hideLoader();

      //  ProgressTask.getInstance().closeProgress();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, bankMainDO);
        }
    }
}