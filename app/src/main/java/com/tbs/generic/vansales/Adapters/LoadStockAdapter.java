package com.tbs.generic.vansales.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;


import com.tbs.generic.vansales.Model.LoadStockDO;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.common.AppConstants;
import com.tbs.generic.vansales.listeners.LoadStockListener;

import java.util.ArrayList;

public class LoadStockAdapter extends RecyclerView.Adapter<LoadStockAdapter.MyViewHolder> implements Filterable {
    private ArrayList<LoadStockDO> loadStockDOS;
    private String imageURL;
    private double weight=0;
    private double volume=0;
    ValueFilter valueFilter;
    private Context context;
    private LoadStockListener loadStockListener;
    double    mass =0.0;
//
//    @Override
//    public Filter getFilter() {
//        return new Filter() {
//            @Override
//            protected FilterResults performFiltering(CharSequence charSequence) {
//                String charString = charSequence.toString();
//                if (charString.isEmpty()) {
//                    loadStockDOS=loadStockDOS;
//                } else {
//                    ArrayList<LoadStockDO> filteredList = new ArrayList<>();
//                    for (LoadStockDO row : loadStockDOS) {
//
//                        // name match condition. this might differ depending on your requirement
//                        // here we are looking for name or phone number match
//                        if (row.product.toLowerCase().contains(charString.toLowerCase())) {
//                            filteredList.add(row);
//                        }
//                    }
//
//                    loadStockDOS = filteredList;
//                }
//
//                FilterResults filterResults = new FilterResults();
//                filterResults.values = loadStockDOS;
//                return filterResults;
//            }
//
//            @Override
//            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
//                loadStockDOS = (ArrayList<LoadStockDO>) filterResults.values;
//                notifyDataSetChanged();
//
//            }
//        };
//    }

    @Override
    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }

    private class ValueFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {
                ArrayList<LoadStockDO> filterList = new ArrayList<>();
                for (int i = 0; i < loadStockDOS.size(); i++) {
                    if ((loadStockDOS.get(i).product.toUpperCase()).contains(constraint.toString().toUpperCase())) {
                        filterList.add(loadStockDOS.get(i));
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            } else {
                results.count = loadStockDOS.size();
                results.values = loadStockDOS;
            }
            return results;

        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            loadStockDOS = (ArrayList<LoadStockDO>) results.values;
            notifyDataSetChanged();
        }

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvProductName, tvDescription,tvNumber;

        public MyViewHolder(View view) {
            super(view);
            tvProductName   = view.findViewById(R.id.tvName);
            tvDescription   = view.findViewById(R.id.tvDescription);
            tvNumber        = view.findViewById(R.id.tvNumber);
        }
    }

    public void refreshAdapter(ArrayList<LoadStockDO> loadStockDoS){
        this.loadStockDOS = loadStockDoS;
        notifyDataSetChanged();

    }
    private String from = "";
    public LoadStockAdapter(Context context, ArrayList<LoadStockDO> loadStockDoS, String from) {
        this.context = context;
        this.loadStockDOS = loadStockDoS;
        this.from = from;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.load_vehicle_stock_layout, parent, false);
        return new MyViewHolder(itemView);
    }

    private ArrayList<LoadStockDO> selectedLoadStockDOs = new ArrayList<>();
    public ArrayList<LoadStockDO> getSelectedLoadStockDOs(){
        return selectedLoadStockDOs;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        if ((position % 2 == 0)) {
            holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.light_grey));
        } else {
            holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.light_secnd_grey));
        }

        final LoadStockDO loadStockDO = loadStockDOS.get(position);
        holder.tvProductName.setText(loadStockDO.product);
        holder.tvDescription.setText(loadStockDO.productDescription );
        holder.tvNumber.setText(""+loadStockDO.quantity+"  "+loadStockDO.weightUnit);
    }

    @Override
    public int getItemCount() {
        return loadStockDOS!=null && loadStockDOS.size()>0?loadStockDOS.size():0;
    }

    private int getCount(){
        int count = 0;
        for(LoadStockDO loadStockDO : AppConstants.listStockDO){
            if(loadStockDO.quantity > 0){
                count = count + 1;
            }
        }
        return count;
    }

    public void filterList(ArrayList<LoadStockDO> filterdNames) {
        this.loadStockDOS = filterdNames;
        notifyDataSetChanged();
    }


}
