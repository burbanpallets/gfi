package com.tbs.generic.pod.Activitys

import android.content.Intent
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.tbs.generic.vansales.Activitys.*
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.utils.Util
import kotlinx.android.synthetic.main.include_toolbar.*


class MasterDashboardActivity : BaseActivity() {


    override fun initialize() {
        var llCategories = layoutInflater.inflate(R.layout.master_data_layout, null) as LinearLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        disableMenuWithBackButton()
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            finish()
//            overridePendingTransition(R.anim.exit, R.anim.enter)
        }
        flToolbar.visibility = View.GONE

        changeLocale()
        setSupportActionBar(act_toolbar)
        Util.getActionBarView(supportActionBar)
        tv_title.text = getString(R.string.update_location)
        act_toolbar.setNavigationOnClickListener {
            Util.preventTwoClick(it)
            finish()
        }
        initializeControls()

    }


    override fun initializeControls() {

        tvScreenTitle.text = getString(R.string.update_location)
        var llCustomers = findViewById<View>(R.id.llCustomers) as LinearLayout
        var llSuppliars = findViewById<View>(R.id.llSuppliars) as LinearLayout
        var llDriver = findViewById<View>(R.id.llDriver) as LinearLayout


        llDriver.setOnClickListener {
            val intent = Intent(this@MasterDashboardActivity, MasterDataActivity::class.java)
            intent.putExtra("TYPE",1)

            startActivity(intent)
        }
        llCustomers.setOnClickListener {
            val intent = Intent(this@MasterDashboardActivity, MasterDataActivity::class.java)
            intent.putExtra("TYPE",2)
            startActivity(intent)
        }
        llSuppliars.setOnClickListener {
            val intent = Intent(this@MasterDashboardActivity, MasterDataActivity::class.java)
            intent.putExtra("TYPE",3)

            startActivity(intent)
        }




    }

    override fun onResume() {
        super.onResume()
    }


}