package com.tbs.generic.vansales.collector;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.UnPaidInvoiceDO;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.utils.PreferenceUtils;

import java.util.ArrayList;

public class CreatePaymentAdapter extends RecyclerView.Adapter<CreatePaymentAdapter.MyViewHolder> {

    private ArrayList<UnPaidInvoiceDO> siteDOS;
    private Context context;
    private String customerId, from;

    private ArrayList<UnPaidInvoiceDO> selectedUnPaidInvoiceDOS = new ArrayList<>();

    public CreatePaymentAdapter(Context context, ArrayList<UnPaidInvoiceDO> siteDOS, String customerID, String froM) {
        this.context = context;
        this.siteDOS = siteDOS;
        this.customerId = customerID;
        this.from = froM;
        calcTotal(siteDOS);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.collector_invoice_data, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final UnPaidInvoiceDO unPaidInvoiceDO = siteDOS.get(position);
        if (from.equalsIgnoreCase("CREDIT")) {
            holder.cbSelected.setVisibility(View.VISIBLE);
        } else {
            holder.cbSelected.setVisibility(View.GONE);
        }
        PreferenceUtils preferenceUtils = new PreferenceUtils(context);

        preferenceUtils.saveString(PreferenceUtils.CURRENCY, "" + unPaidInvoiceDO.outstandingAmountCurrency);

        holder.cbSelected.setOnCheckedChangeListener(null);
        holder.cbSelected.setChecked(unPaidInvoiceDO.isSelected);
//        if (unPaidInvoiceDO.isSelected) {
//            holder.cbSelected.setChecked(true);
//            ((InvoiceListActivity) context).recycleview.smoothScrollToPosition(siteDOS.size() - 1);

//            ((BaseActivity) context).ivSelectAll.setImageDrawable(context.getResources().getDrawable(R.drawable.selected));
//
//            final double[] total = {0.0};
//
//            selectedUnPaidInvoiceDOS.add(unPaidInvoiceDO);
//            for (int i = 0; i < selectedUnPaidInvoiceDOS.size(); i++) {
//                total[0] = total[0] + selectedUnPaidInvoiceDOS.get(i).outstandingAmount;
//
//
////                        selectedUnPaidInvoiceDOS.get(i).outstandingAmount=   selectedUnPaidInvoiceDOS.get(i).outstandingAmount+selectedUnPaidInvoiceDOS.get(i).outstandingAmount;
//
//            }
//            ((com.tbs.generic.vansales.collector.InvoiceListActivity) context).tvPaidAmount.setText("Being Paid      : " + String.format("%.2f", total[0]) + " " + selectedUnPaidInvoiceDOS.get(0).currency);
//            ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.CREDIT_INVOICE_AMOUNT, "" + total[0]);

//        } else {
//            holder.cbSelected.setChecked(false);
//            ((InvoiceListActivity) context). recycleview.smoothScrollToPosition(siteDOS.size() - 1);

//            ((BaseActivity) context).ivSelectAll.setImageDrawable(context.getResources().getDrawable(R.drawable.select_all));
//
//            final double[] total = {0.0};
//
//            selectedUnPaidInvoiceDOS.remove(unPaidInvoiceDO);
////                    selectedUnPaidInvoiceDOS.add(unPaidInvoiceDO);
//            for (int i = 0; i < selectedUnPaidInvoiceDOS.size(); i++) {
//                total[0] = total[0] + selectedUnPaidInvoiceDOS.get(i).outstandingAmount;
//
//
////                        selectedUnPaidInvoiceDOS.get(i).outstandingAmount=   selectedUnPaidInvoiceDOS.get(i).outstandingAmount-selectedUnPaidInvoiceDOS.get(i).outstandingAmount;
//
//            }
//            if (total[0] > 0) {
//                ((com.tbs.generic.vansales.collector.InvoiceListActivity) context).tvPaidAmount.setText("Being Paid       " + String.format("%.2f", total[0]) + " " + selectedUnPaidInvoiceDOS.get(0).currency);
//                ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.CREDIT_INVOICE_AMOUNT, "" + total[0]);
//
//            } else {
//                ((com.tbs.generic.vansales.collector.InvoiceListActivity) context).tvPaidAmount.setText("Being Paid      : 0");
//                ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.CREDIT_INVOICE_AMOUNT, "" + 0);
//
//            }

//        }
        holder.cbSelected.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                unPaidInvoiceDO.isSelected = isChecked;
                calcTotal(siteDOS);
            }
        });
        double invoicedAmount = Double.parseDouble(String.format("%.2f", unPaidInvoiceDO.paidAmount + unPaidInvoiceDO.outstandingAmount));

        if (unPaidInvoiceDO.accountingDate.length() > 0) {
            String aMonth = unPaidInvoiceDO.accountingDate.substring(4, 6);
            String ayear = unPaidInvoiceDO.accountingDate.substring(0, 4);
            String aDate = unPaidInvoiceDO.accountingDate.substring(Math.max(unPaidInvoiceDO.accountingDate.length() - 2, 0));
            holder.tvSiteName.setText("Invoice  No            :  " + unPaidInvoiceDO.invoiceId + "\n" + "Accounting Date  :  " + aDate + "-" + aMonth + "-" + ayear
                    + "\n" + "Invoiced Amount :  " + invoicedAmount + " " + unPaidInvoiceDO.currency
                    + "\n" + "Paid Amount        :  " + String.format("%.2f", unPaidInvoiceDO.paidAmount) + " " + unPaidInvoiceDO.paidAmountCurrency
                    + "\n" + "Balance Amount  :  " + String.format("%.2f", unPaidInvoiceDO.outstandingAmount) + " " + unPaidInvoiceDO.outstandingAmountCurrency
                    + "\n" + "Due Days               :  " + unPaidInvoiceDO.dueDays

            );
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                int pos = holder.getAdapterPosition();
//                PreferenceUtils preferenceUtils = new PreferenceUtils(context);
//                preferenceUtils.saveString(PreferenceUtils.INVOICE_ID, String.valueOf(siteDOS.get(pos).invoiceId));
//                preferenceUtils.saveString(PreferenceUtils.INVOICE_AMOUNT, "" + unPaidInvoiceDO.outstandingAmount);
//                preferenceUtils.saveString(PreferenceUtils.CURRENCY, "" + unPaidInvoiceDO.outstandingAmountCurrency);
////                ((CreatePaymentActivity) context).etAmount.setText("" + unPaidInvoiceDO.outstandingAmount);
////                ((CreatePaymentActivity) context).tvSelection.setText("" + preferenceUtils.getStringFromPreference(PreferenceUtils.INVOICE_ID, ""));
////                ((CreatePaymentActivity) context).btnPayments.setBackgroundColor(ContextCompat.getColor(context, R.color.md_green));
////                ((CreatePaymentActivity) context).btnPayments.setClickable(true);
////                ((CreatePaymentActivity) context).btnPayments.setEnabled(true);
////                ((CreatePaymentActivity) context).dialog.dismiss();
//
//                Intent intent = new Intent(context, CreatePaymentActivity.class);
//                intent.putExtra("INVOICE_ID", String.valueOf(siteDOS.get(pos).invoiceId));
//                intent.putExtra("INVOICE_AMOUNT", "" + unPaidInvoiceDO.outstandingAmount);
//                intent.putExtra("CURRENCY", "" + unPaidInvoiceDO.outstandingAmountCurrency);
//                intent.putExtra("SINGLE", "PAY");
//                intent.putExtra("CODE", customer);
//
//                ((com.tbs.generic.vansales.collector.InvoiceListActivity) context).startActivityForResult(intent, 1);


            }
        });
//        holder.cbSelected.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (isChecked) {
//                    final double[] total = {0.0};
//                    selectedUnPaidInvoiceDOS.add(unPaidInvoiceDO);
////                    for (int i = 0; i < selectedUnPaidInvoiceDOS.size(); i++) {
////                        total[0] = total[0] + selectedUnPaidInvoiceDOS.get(i).outstandingAmount;
////
////
//////                        selectedUnPaidInvoiceDOS.get(i).outstandingAmount=   selectedUnPaidInvoiceDOS.get(i).outstandingAmount+selectedUnPaidInvoiceDOS.get(i).outstandingAmount;
////
////                    }
////                    ((com.tbs.generic.vansales.collector.InvoiceListActivity) context).tvPaidAmount.setText("Being Paid      : " + String.format("%.2f", total[0]) + " " + selectedUnPaidInvoiceDOS.get(0).currency);
////                    ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.CREDIT_INVOICE_AMOUNT, "" + total[0]);
//                    calcTotal(selectedUnPaidInvoiceDOS);
//                } else {
//                    final double[] total = {0.0};
//
//                    selectedUnPaidInvoiceDOS.remove(unPaidInvoiceDO);
////                    selectedUnPaidInvoiceDOS.add(unPaidInvoiceDO);
////                    for (int i = 0; i < selectedUnPaidInvoiceDOS.size(); i++) {
////                        total[0] = total[0] + selectedUnPaidInvoiceDOS.get(i).outstandingAmount;
////
////
//////                        selectedUnPaidInvoiceDOS.get(i).outstandingAmount=   selectedUnPaidInvoiceDOS.get(i).outstandingAmount-selectedUnPaidInvoiceDOS.get(i).outstandingAmount;
////
////                    }
////                    if (total[0] > 0) {
////                        ((com.tbs.generic.vansales.collector.InvoiceListActivity) context).tvPaidAmount.setText("Being Paid       " + String.format("%.2f", total[0]) + " " + selectedUnPaidInvoiceDOS.get(0).currency);
////                        ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.CREDIT_INVOICE_AMOUNT, "" + total[0]);
////
////                    } else {
////                        ((com.tbs.generic.vansales.collector.InvoiceListActivity) context).tvPaidAmount.setText("Being Paid      : 0");
////                        ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.CREDIT_INVOICE_AMOUNT, "" + 0);
////
////                    }
//                    calcTotal(selectedUnPaidInvoiceDOS);
//                }
//            }
//        });
    }

    private double outstandingAmount;
    private void calcTotal(ArrayList<UnPaidInvoiceDO>  unPaidInvoiceDOS){
        outstandingAmount = 0;
        for (int i = 0; i < unPaidInvoiceDOS.size(); i++) {
            if (unPaidInvoiceDOS.get(i).isSelected) {
                outstandingAmount = outstandingAmount + unPaidInvoiceDOS.get(i).outstandingAmount;
                selectedUnPaidInvoiceDOS.add(unPaidInvoiceDOS.get(i));

            }else {
                selectedUnPaidInvoiceDOS.remove(unPaidInvoiceDOS.get(i));

            }
        }
        if(unPaidInvoiceDOS.size()>0){

            ((com.tbs.generic.vansales.collector.InvoiceListActivity) context).tvPaidAmount.setText("Being Paid      : "+outstandingAmount);
            ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.CREDIT_INVOICE_AMOUNT, "" + outstandingAmount);
        }else {
//            ((com.tbs.generic.vansales.collector.InvoiceListActivity) context).tvPaidAmount.setVisibility(View.GONE);
//            ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.CREDIT_INVOICE_AMOUNT, "" + outstandingAmount);

        }
    }

    @Override
    public int getItemCount() {
        return siteDOS.size();
    }

    public ArrayList<UnPaidInvoiceDO> getselectedUnPaidInvoiceDOS() {
        return selectedUnPaidInvoiceDOS;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvSiteName, tvSiteId;
        private LinearLayout llDetails;
        private CheckBox cbSelected;

        public MyViewHolder(View view) {
            super(view);
            llDetails = view.findViewById(R.id.llDetails);
            tvSiteName = view.findViewById(R.id.tvInvoiceNumber);
            tvSiteId = view.findViewById(R.id.tvSiteId);
            cbSelected = itemView.findViewById(R.id.cbSelected);
        }
    }


}
