package com.tbs.generic.vansales.pdfs.utils;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.widget.LinearLayout;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;
import com.shockwave.pdfium.PdfDocument;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.utils.Util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class PreviewActivity extends AppCompatActivity implements OnPageChangeListener,OnLoadCompleteListener {
    PDFView pdfView;
    LinearLayout ll1;
    Integer pageNumber = 0;
    String pdfFileName;
    String TAG="PdfActivity";
    int position=-1;
    public static ArrayList<File> fileList = new ArrayList<File>();
    File dir;
    String name;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf);
        init();
    }

    private void init(){
        pdfView= findViewById(R.id.pdfView);
        ll1= findViewById(R.id.ll1);

//        position = getIntent().getIntExtra("position",-1);
        displayFromSdcard();
    }

    private void displayFromSdcard() {
        if (getIntent().hasExtra("KEY")) {
            pdfFileName = getIntent().getExtras().getString("KEY");
        }
//        pdfFileName = PDFConstants.CYL_DELIVERY_NOTE_PDF_NAME;
        dir = new File(Util.getAppPath(PreviewActivity.this));

        getfile(dir);

//        File file= new File(String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath().endsWith(pdfFileName)));
        for (int j = 0; j < fileList.size(); j++) {
            if(fileList.get(j).getAbsoluteFile().getName().equals(pdfFileName)){
                pdfView.fromFile(fileList.get(j).getAbsoluteFile())
                        .defaultPage(pageNumber)
                        .enableSwipe(true)

                        .swipeHorizontal(false)
                        .onPageChange(this)
                        .enableAnnotationRendering(true)
                        .onLoad(this)
                        .scrollHandle(new DefaultScrollHandle(this))
                        .load();
            }
            }


    }


    public ArrayList<File> getfile(File dir) {

        File[] listFile = dir.listFiles();
        int i=0;
        if (listFile != null && listFile.length > 0) {
            for ( i = 0; i < listFile.length; i++) {

                if (listFile[i].isDirectory()) {
                    getfile(listFile[i]);

                } else {

                    boolean booleanpdf = false;
                    if (listFile[i].getName().endsWith(pdfFileName)) {

                        for (int j = 0; j < fileList.size(); j++) {
                            if (fileList.get(j).getName().equals(listFile[i].getName())) {
                                booleanpdf = true;
                            } else {

                            }
                        }

                        if (booleanpdf) {
                            booleanpdf = false;
                        } else {
                            fileList.add(listFile[i]);

                        }
                    }
                }
            }
        }
        return fileList;
    }
    @Override
    public void onPageChanged(int page, int pageCount) {
        pageNumber = page;
        setTitle(String.format("%s %s / %s", pdfFileName, page + 1, pageCount));
    }


    @Override
    public void loadComplete(int nbPages) {
        PdfDocument.Meta meta = pdfView.getDocumentMeta();
        printBookmarksTree(pdfView.getTableOfContents(), "-");

    }

    public void printBookmarksTree(List<PdfDocument.Bookmark> tree, String sep) {
        for (PdfDocument.Bookmark b : tree) {

            Log.e(TAG, String.format("%s %s, p %d", sep, b.getTitle(), b.getPageIdx()));

            if (b.hasChildren()) {
                printBookmarksTree(b.getChildren(), sep + "-");
            }
        }
    }
}
