package com.tbs.generic.vansales.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.tbs.generic.vansales.Activitys.BarcodeAddActivity;
import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Activitys.SerializationActivity;
import com.tbs.generic.vansales.Model.TrailerSelectionDO;
import com.tbs.generic.vansales.R;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class EquipProdSelectionAdapter extends RecyclerView.Adapter<EquipProdSelectionAdapter.MyViewHolder> {

    private ArrayList<TrailerSelectionDO> trailerSelectionDOS;
    private Context context;


    public void refreshAdapter(@NotNull ArrayList<TrailerSelectionDO> trailerSelectionDOS) {
        this.trailerSelectionDOS = trailerSelectionDOS;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvPO, tvLoan, tvCustomer, tvEquiment, tvQty;
        private CheckBox cbSelected;
        private Button btnSelect;

        public MyViewHolder(View view) {
            super(view);

            tvEquiment = itemView.findViewById(R.id.tvEquioment);
            tvQty = itemView.findViewById(R.id.tvQty);
            btnSelect = itemView.findViewById(R.id.btnSelect);


        }
    }


    public EquipProdSelectionAdapter(Context context, ArrayList<TrailerSelectionDO> trailerSelectionDOS) {
        this.context = context;
        this.trailerSelectionDOS = trailerSelectionDOS;

    }

    private ArrayList<TrailerSelectionDO> selectedDOs = new ArrayList<>();

    public ArrayList<TrailerSelectionDO> getSelectedTrailerDOs() {
        return selectedDOs;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.prod_eqip_data, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final TrailerSelectionDO trailerSelectionDO = trailerSelectionDOS.get(position);
//        final VRSelectionDO vrSelectionDO = new VRSelectionDO();
        if ((position % 2 == 0)) {
            holder.itemView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.edit_text_background_lg));
        } else {
            holder.itemView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.edit_text_background_lsg));
        }


        holder.tvEquiment.setText(trailerSelectionDO.trailer+" - "+trailerSelectionDO.trailerDescription);

        holder.tvQty.setText(""+trailerSelectionDO.quantity);


        holder.btnSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent= new Intent(context, BarcodeAddActivity.class);
                Intent intent= new Intent(context, SerializationActivity.class);
                intent.putExtra("PRODUCTDO", trailerSelectionDO);
                intent.putExtra("POSITION", position);

                ((BaseActivity) context).startActivityForResult(intent,10);


            }
        });

    }

    @Override
    public int getItemCount() {
        return trailerSelectionDOS.size();
//        return 10;

    }

}
