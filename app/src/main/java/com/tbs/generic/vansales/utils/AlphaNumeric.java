package com.tbs.generic.vansales.utils;

import android.widget.Toast;

import com.tbs.generic.vansales.Activitys.BaseActivity;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

public class AlphaNumeric {

    // This is my number set it contains 62 symbols
    private final List<Character> numberset = Arrays.asList(
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'
    );
    private String initAL; // keep starting AN
    private int BASE; // base of numberset is 62 so, addition = (anynumber %
    // BASE) and spare = (anynumber / 62)

    public AlphaNumeric(String initAL) {
        // initializing the values
        this.initAL = initAL;
        this.BASE = numberset.size();
    }

    public synchronized String nextAN() {

        char[] number = initAL.toCharArray();
        int spare = 0;
        int addition = 1;

        /*
         * Here what i'm doing is i'm looping the given string backwards and get
         * char by char
         */

        for (int i = number.length - 1; i >= 0; i--) {

            int lastnumber = numberset.indexOf(number[i]);
            /*
             * abov i'm getting number associated with the last char, example
             * letter "A" means 10 and in the below line i'm adding +1, also if
             * there is number left in the previous calculation I'm adding it
             * too
             */

            int newnumb = lastnumber + addition + spare;

            /*
             * now i'm checking whether the new number is exceeding the base,
             * example, in normal number set is 9+1 = 10
             */
            if (newnumb >= BASE) {
                // calculate spare and the addition
                number[i] = numberset.get(newnumb % BASE);
                spare = newnumb / BASE;
            } else {
				/*
				if the addition is not exceeding the base then we can just
				add them
				and stop there
				*/
                number[i] = numberset.get(newnumb);
                break;
            }
			/*
			checkin a special situation, spare can be 1 but sometimes number
			might have end in the next iteration, in this case i'm adding the
			spare to the front
			and stoping
			*/
            if ((spare > 0) && ((i - 1) < 0)) {
                number = (numberset.get(spare) + new String(number)).toCharArray();
                break;
            }
        }

        return initAL = new String(number);
    }

    public static void main(String[] args) {
        AlphaNumeric docid = new AlphaNumeric("00");
        int counter = 0;
        while (true) {
            ++counter;
            if (counter == 4000) {
                break;
            }
            System.out.println("ID: " + docid.nextAN() + " Counter :" + counter);
        }
    }

    public static void writeline(String txt){

        try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("keys.txt", true)))) {
            out.println(txt);
        }catch (IOException e) {
            e.printStackTrace();
        }
    }

}
