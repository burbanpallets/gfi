package com.tbs.generic.vansales.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class TestMainDO implements Serializable {


    public String type = "";
    public String id = "";
    public String message = "";
    public String country = "";
    public String sunrise = "";
    public String sunset = "";

    public Double lat = 0.0;
    public Double lon = 0.0;

    public String temp = "";
    public String humidity = "";
    public String pressure = "";
    public String temp_min = "";
    public String temp_max = "";
    public String name = "";

    public ArrayList<TestDO> testDOS = new ArrayList<>();


}
