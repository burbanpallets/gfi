package com.tbs.generic.vansales.Activitys

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.tbs.generic.vansales.Adapters.UnLoadStockAdapter
import com.tbs.generic.vansales.Model.LoadStockDO
import com.tbs.generic.vansales.R
import java.util.ArrayList

//
class UnloadVanSalesStockActivity : BaseActivity() {
    lateinit var recycleview : androidx.recyclerview.widget.RecyclerView
    lateinit var loadStockAdapter: UnLoadStockAdapter
    lateinit var loadStockDOs: ArrayList<LoadStockDO>

    override fun onResume() {
        super.onResume()
    }
    override fun initialize() {
      var  llCategories = layoutInflater.inflate(R.layout.unload_van_sale_stock_screen, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        initializeControls()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
    }
    override fun initializeControls() {
        tvScreenTitle.setText(R.string.unload_vansale_stock)
        recycleview = findViewById<androidx.recyclerview.widget.RecyclerView>(R.id.recycleview)
        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)

        recycleview.layoutManager = linearLayoutManager
        loadStockAdapter = UnLoadStockAdapter(this@UnloadVanSalesStockActivity,  null)
        recycleview.adapter = loadStockAdapter
    }
}