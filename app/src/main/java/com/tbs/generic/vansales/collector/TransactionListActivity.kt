package com.tbs.generic.vansales.collector

import android.app.DatePickerDialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tbs.generic.vansales.Activitys.BaseActivity
import com.tbs.generic.vansales.Adapters.InvoiceAdapter
import com.tbs.generic.vansales.Model.LoadStockDO
import com.tbs.generic.vansales.Model.PaymentPdfDO
import com.tbs.generic.vansales.Model.TransactionCashMainDo
import com.tbs.generic.vansales.Model.TransactionChequeMainDo
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.PaymentChequeTransactionListRequest
import com.tbs.generic.vansales.Requests.PaymentTransactionListRequest
import com.tbs.generic.vansales.common.AppConstants
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.pdfs.ReceiptPDF
import com.tbs.generic.vansales.prints.PrinterConnection
import com.tbs.generic.vansales.prints.PrinterConstants
import com.tbs.generic.vansales.utils.CalendarUtils
import com.tbs.generic.vansales.utils.PreferenceUtils
import com.tbs.generic.vansales.utils.Util
import java.io.Serializable
import java.util.*
import kotlin.collections.ArrayList


class TransactionListActivity : BaseActivity() {
    lateinit var invoiceAdapter: InvoiceAdapter
    lateinit var loadStockDOs: ArrayList<LoadStockDO>
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var recycleview2: androidx.recyclerview.widget.RecyclerView
    lateinit var btnCreate: Button
    private var transactionCashMainDO:TransactionCashMainDo = TransactionCashMainDo()
    private var transactionChequeMainDO:TransactionChequeMainDo = TransactionChequeMainDo()

    lateinit var tvNoDataFound: TextView
    var fromId = 0
    var startDate = ""
    var endDate = ""
    var from = ""
    var to = ""
    lateinit var tvTotalAmount: TextView
    lateinit var tvCashReciepts: TextView
    lateinit var tvChequeReciepts: TextView
    lateinit var tvTotalChequeAmount: TextView
    private lateinit var siteListRequest: PaymentTransactionListRequest

    private var cday: Int = 0
    private var cmonth: Int = 0
    private var cyear: Int = 0
    lateinit var tvFrom: TextView
    lateinit var tvTO: TextView
    lateinit var tvUserId: TextView
    lateinit var tvUserName: TextView

    lateinit var llItem1: LinearLayout
    lateinit var llItem2: LinearLayout
    lateinit var llCash: LinearLayout
    lateinit var llCheque: LinearLayout
    lateinit var llCashAmount: LinearLayout
    lateinit var llChequeAmount: LinearLayout
    lateinit var llUserId: LinearLayout
    lateinit var llName: LinearLayout
    lateinit var view1: View
    lateinit var view2: View
    lateinit var view3: View
    lateinit var view4: View
    lateinit var view5: View
    override fun initialize() {
        var llCategories = layoutInflater.inflate(R.layout.transactions_list, null) as View
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
    }

    override fun initializeControls() {
        val name = preferenceUtils.getStringFromPreference(PreferenceUtils.LOGIN_DRIVER_NAME, "")
        val id = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "")

        tvScreenTitle.text = name
        recycleview = findViewById<androidx.recyclerview.widget.RecyclerView>(R.id.recycleview)
        recycleview2 = findViewById<androidx.recyclerview.widget.RecyclerView>(R.id.recycleview2)
        recycleview.isNestedScrollingEnabled = false
        recycleview2.isNestedScrollingEnabled = false
        view1 = findViewById<View>(R.id.view1)
        view2 = findViewById<View>(R.id.view2)
        view3 = findViewById<View>(R.id.view3)
        view4 = findViewById<View>(R.id.view4)
        view5 = findViewById<View>(R.id.view5)

        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        tvNoDataFound = findViewById<TextView>(R.id.tvNoData)
        tvFrom = findViewById<TextView>(R.id.tvFrom)
        tvTO = findViewById<TextView>(R.id.tvTO)
        tvTotalAmount = findViewById<TextView>(R.id.tvTotalAmount)
        tvCashReciepts = findViewById<TextView>(R.id.tvCashReciepts)
        tvChequeReciepts = findViewById<TextView>(R.id.tvChequeReciepts)
        tvTotalChequeAmount = findViewById<TextView>(R.id.tvTotalChequeAmount)
        tvUserId = findViewById<TextView>(R.id.tvUserId)
        tvUserName = findViewById<TextView>(R.id.tvUserName)
        tvUserId.text = id
        tvUserName.text = name
        llUserId = findViewById<LinearLayout>(R.id.llUserId)
        llName = findViewById<LinearLayout>(R.id.llName)
        llItem1 = findViewById<LinearLayout>(R.id.llItem)
        llItem2 = findViewById<LinearLayout>(R.id.llItem2)
        llCash = findViewById<LinearLayout>(R.id.llCash)
        llCheque = findViewById<LinearLayout>(R.id.llCheque)
        llCashAmount = findViewById<LinearLayout>(R.id.llCashAmount)
        llChequeAmount = findViewById<LinearLayout>(R.id.llChequeAmount)
       var from= CalendarUtils.getDate()

        val aMonth = from.substring(4, 6)
        val ayear  = from.substring(0, 4)
        val aDate  = from.substring(Math.max(from.length - 2, 0))


        tvFrom.text = "" + aDate + " - " + aMonth + " - " + ayear
        tvTO.text = "" +aDate + " - " + aMonth + " - " + ayear
        startDate=ayear+aMonth+aDate
        endDate=ayear+aMonth+aDate

        if (intent.hasExtra("Sales")) {
            fromId = intent.extras!!.getInt("Sales")
        }
        if (intent.hasExtra("START")) {
            startDate = intent.extras?.getString("START")!!
        }
        if (intent.hasExtra("END")) {
            endDate = intent.extras?.getString("END")!!
        }
        if (intent.hasExtra("FROM")) {
            from = intent.extras!!.getString("FROM")
        }
        if (intent.hasExtra("TO")) {
            to = intent.extras?.getString("TO")!!
        }
//        tvFrom.setText("From : "+from)
//        tvTO.setText("To : "+to)
        recycleview.layoutManager = linearLayoutManager


        btnCreate = findViewById<Button>(R.id.btnCreate)
        val btnSearch = findViewById<Button>(R.id.btnSearch)

        btnSearch.setOnClickListener {
            transactionCashMainDO.transactionCashDOS.clear()
            transactionChequeMainDO.transactionChequeDOS.clear()
            var frm = tvFrom.text.toString().trim()
            var to   =tvTO.text.toString().trim()
            if(frm.isNotEmpty()&& to.isNotEmpty()){

                selectInvoiceList()
            }
            else{
                showToast("Please select From and To dates")
            }
        }
        val c = Calendar.getInstance()
        val cyear = c.get(Calendar.YEAR)
        val cmonth = c.get(Calendar.MONTH)
        val cday = c.get(Calendar.DAY_OF_MONTH)
        tvFrom.setOnClickListener {
            val datePicker = DatePickerDialog(this@TransactionListActivity, datePickerListener, cyear, cmonth, cday)

            datePicker.show()
        }

        tvTO.setOnClickListener {
            val datePicker = DatePickerDialog(this@TransactionListActivity, endDatePickerListener, cyear, cmonth, cday)

            datePicker.show()
        }

        btnCreate.setOnClickListener {

            prepareActivityReport()
        }

    }

    private fun selectInvoiceList() {


//        var tvInvoiceId = findViewById(R.id.tvInvoiceId) as TextView
        var tvNoData = findViewById<TextView>(R.id.tvNoData)


        recycleview.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        recycleview2.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)

        if (Util.isNetworkAvailable(this)) {
            siteListRequest = PaymentTransactionListRequest(startDate, endDate, this@TransactionListActivity)

            siteListRequest.setOnResultListener { isError, unPaidInvoiceMainDO ->
                hideLoader()
                if (unPaidInvoiceMainDO != null && unPaidInvoiceMainDO.transactionCashDOS.size > 0) {

                    if (isError) {
                        recycleview.visibility = View.GONE
                        llItem1.visibility = View.GONE
                        llCash.visibility = View.GONE
                        llCashAmount.visibility = View.GONE
                        view1.visibility = View.GONE
                        view2.visibility = View.GONE
                        view3.visibility = View.GONE
                        if(transactionChequeMainDO.transactionChequeDOS.size>0){
                            btnCreate.visibility = View.VISIBLE
                            tvNoData.visibility = View.GONE
                        }

//                    if(btnCreate.visibility != View.VISIBLE){
//                        btnCreate.setVisibility(View.GONE)
//                    }
                        Toast.makeText(this@TransactionListActivity, resources.getString(R.string.error_NoData), Toast.LENGTH_SHORT).show()
                    } else {
                        transactionCashMainDO=unPaidInvoiceMainDO
                        llItem1.visibility = View.VISIBLE
                        llCash.visibility = View.VISIBLE
                        llCashAmount.visibility = View.VISIBLE
                        view1.visibility = View.VISIBLE
                        view2.visibility = View.VISIBLE
                        view3.visibility = View.VISIBLE
                        recycleview.visibility = View.VISIBLE
                        btnCreate.visibility = View.VISIBLE
                        tvNoData.visibility = View.GONE
                        var siteAdapter = TransactionPaymentAdapter(this@TransactionListActivity, unPaidInvoiceMainDO.transactionCashDOS, "")
                        recycleview.adapter = siteAdapter
                        tvCashReciepts.text = "" + unPaidInvoiceMainDO.cashReciepts
                        tvTotalAmount.text = "" + unPaidInvoiceMainDO.totalAmount+" AED"
                        llName.visibility = View.VISIBLE
                        llUserId.visibility = View.VISIBLE

                    }
                } else {

                    recycleview.visibility = View.GONE
                    llItem1.visibility = View.GONE
                    llCash.visibility = View.GONE
                    llCashAmount.visibility = View.GONE
                    view1.visibility = View.GONE
                    view2.visibility = View.GONE
                    view3.visibility = View.GONE
                    if(transactionChequeMainDO.transactionChequeDOS.size>0){
                        btnCreate.visibility = View.VISIBLE
                        tvNoData.visibility = View.GONE
                    }
                    else{
                        btnCreate.visibility = View.GONE
                        tvNoData.visibility = View.VISIBLE
                    }
//                if(btnCreate.visibility != View.VISIBLE){
//                    btnCreate.setVisibility(View.GONE)
//                }
                    hideLoader()

                }

            }

            siteListRequest.execute()
        } else {
//            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), "OK", "", "",false)
           showToast(""+resources.getString(R.string.internet_connection))
        }

        if (Util.isNetworkAvailable(this)) {
            var siteListRequet = PaymentChequeTransactionListRequest(startDate, endDate, this@TransactionListActivity)

            siteListRequet.setOnResultListener { isError, unPaidInvoiceMainDO ->
                hideLoader()
                if (unPaidInvoiceMainDO != null && unPaidInvoiceMainDO.transactionChequeDOS.size > 0) {

                    if (isError) {
                        recycleview2.visibility = View.GONE
                        view4.visibility = View.GONE
                        view5.visibility = View.GONE
                        if(transactionCashMainDO.transactionCashDOS.size>0){
                            btnCreate.visibility = View.VISIBLE
                            tvNoData.visibility = View.GONE
                        }
                        llCheque.visibility = View.GONE
                        llItem2.visibility = View.GONE
                        llChequeAmount.visibility = View.GONE


                        Toast.makeText(this@TransactionListActivity, resources.getString(R.string.error_NoData), Toast.LENGTH_SHORT).show()
                    } else {
                        transactionChequeMainDO=unPaidInvoiceMainDO
                        tvNoData.visibility = View.GONE
                        view4.visibility = View.VISIBLE
                        view5.visibility = View.VISIBLE
                        btnCreate.visibility = View.VISIBLE
                        llCheque.visibility = View.VISIBLE
                        llItem2.visibility = View.VISIBLE
                        llChequeAmount.visibility = View.VISIBLE
                        recycleview2.visibility = View.VISIBLE
                        var siteAdapter = TransactionChequePaymentAdapter(this@TransactionListActivity, unPaidInvoiceMainDO.transactionChequeDOS, "")
                        recycleview2.adapter = siteAdapter
                        tvChequeReciepts.text = "" + unPaidInvoiceMainDO.chequeReciepts
                        tvTotalChequeAmount.text = "" + unPaidInvoiceMainDO.totalAmount+" AED"
                        llName.visibility = View.VISIBLE
                        llUserId.visibility = View.VISIBLE

                    }
                } else {

                    recycleview2.visibility = View.GONE
                    view4.visibility = View.GONE
                    view5.visibility = View.GONE

                    llCheque.visibility = View.GONE
                    llItem2.visibility = View.GONE
                    llChequeAmount.visibility = View.GONE
                    if(transactionCashMainDO.transactionCashDOS.size>0){
                        btnCreate.visibility = View.VISIBLE
                        tvNoData.visibility = View.GONE
                    }
                    else{
                        btnCreate.visibility = View.GONE
                        tvNoData.visibility = View.VISIBLE
                    }
                    hideLoader()

                }

            }

            siteListRequet.execute()
        } else {
            showToast(""+resources.getString(R.string.internet_connection))
//            showAppCompatAlert("Alert!", , "OK", "", "",false)

        }



    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == 1) {
//            selectInvoiceList()
        }
    }


    private val datePickerListener: DatePickerDialog.OnDateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        cday = dayOfMonth
        cmonth = monthOfYear + 1
        cyear = year
        var monthString = cmonth.toString()
        if (monthString.length == 1) {
            monthString = "0$monthString"
        } else {
            monthString = cmonth.toString()
        }

        var dayString = cday.toString()
        if (dayString.length == 1) {
            dayString = "0$dayString"
        } else {
            dayString = cday.toString()
        }
        startDate = "" + cyear + monthString + dayString
        tvFrom.text = "" + dayString + " - " + monthString + " - " + cyear
        from = tvFrom.text.toString()

    }
    private val endDatePickerListener: DatePickerDialog.OnDateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        cday = dayOfMonth
        cmonth = monthOfYear + 1
        cyear = year
        var monthString = cmonth.toString()
        if (monthString.length == 1) {
            monthString = "0$monthString"
        } else {
            monthString = cmonth.toString()
        }

        var dayString = cday.toString()
        if (dayString.length == 1) {
            dayString = "0$dayString"
        } else {
            dayString = cday.toString()
        }
        endDate = "" + cyear + monthString + dayString
        tvTO.text = "" + dayString + " - " + monthString + " - " + cyear
        to = tvTO.text.toString()

    }

    private fun prepareActivityReport() {

        val objLlist = ArrayList<Serializable>()
        objLlist.add(transactionCashMainDO)
        objLlist.add(transactionChequeMainDO)
        AppConstants.Trans_From_Date = tvFrom.text.toString().trim().replace("-", "/")
        AppConstants.Trans_To_Date = tvTO.text.toString().trim().replace("-", "/")
        printDocument(objLlist, PrinterConstants.PrintActivityReport)

    }


    private fun printDocument(obj: Any, from: Int) {
        val printConnection = PrinterConnection.getInstance(this@TransactionListActivity)
        if (printConnection.isBluetoothEnabled) {
            printConnection.connectToPrinter(obj, from)
        } else {
//            PrinterConstants.PRINTER_MAC_ADDRESS = ""
            showToast("Please enable your mobile Bluetooth.")
//            showAppCompatAlert("", "Please enable your mobile Bluetooth.", "Enable", "Cancel", "EnableBluetooth", false)
        }
    }

    private fun pairedDevices() {
        val pairedDevices = BluetoothAdapter.getDefaultAdapter().bondedDevices
        if (pairedDevices.size > 0) {
            for (device in pairedDevices) {
                val deviceName = device.name
                val mac = device.address // MAC address
//                if (StorageManager.getInstance(this).getPrinterMac(this).equals("")) {
                StorageManager.getInstance(this).savePrinterMac(this, mac)
//                }
                Log.e("Bluetooth", "Name : " + deviceName + " Mac : " + mac)
                break
            }
        }
    }

    override fun onResume() {
        super.onResume()
        pairedDevices()
    }

    override fun onStart() {
        super.onStart()
        val filter = IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED)
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED)
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED)
        registerReceiver(mReceiver, filter)
//        val filter = IntentFilter(Intent.PAIRIN.ACTION_FOUND Intent. "android.bluetooth.device.action.PAIRING_REQUEST");
//        registerReceiver(mReceiver, filter)
//        val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
//        mBluetoothAdapter.startDiscovery()
//        val filter2 = IntentFilter( "android.bluetooth.device.action.PAIRING_REQUEST")
//        registerReceiver(mReceiver, filter2)

    }

    override fun onDestroy() {
        unregisterReceiver(mReceiver)
//        PrinterConstants.PRINTER_MAC_ADDRESS = ""
        super.onDestroy()
    }

    private val mReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            if (BluetoothDevice.ACTION_FOUND == action) {
                val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE)
                PrinterConstants.bluetoothDevices.add(device)
                Log.e("Bluetooth", "Discovered => name : " + device.name + ", Mac : " + device.address)
            } else if (action.equals(BluetoothDevice.ACTION_ACL_CONNECTED)) {
                Log.e("Bluetooth", "status : ACTION_ACL_CONNECTED")
                pairedDevices()
            } else if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                val state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR)

                if (state == BluetoothAdapter.STATE_OFF) {
                    Log.e("Bluetooth", "status : STATE_OFF")
                } else if (state == BluetoothAdapter.STATE_TURNING_OFF) {
                    Log.e("Bluetooth", "status : STATE_TURNING_OFF")
                } else if (state == BluetoothAdapter.STATE_ON) {
                    Log.e("Bluetooth", "status : STATE_ON")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_TURNING_ON) {
                    Log.e("Bluetooth", "status : STATE_TURNING_ON")
                } else if (state == BluetoothAdapter.STATE_CONNECTING) {
                    Log.e("Bluetooth", "status : STATE_CONNECTING")
                } else if (state == BluetoothAdapter.STATE_CONNECTED) {
                    Log.e("Bluetooth", "status : STATE_CONNECTED")
                    pairedDevices()
                } else if (state == BluetoothAdapter.STATE_DISCONNECTED) {
                    Log.e("Bluetooth", "status : STATE_DISCONNECTED")
                }
            }
        }
    }

    private fun createPaymentPDF(createPDFInvoiceDO: PaymentPdfDO) {
        if (createPDFInvoiceDO != null) {
            ReceiptPDF(this).createReceiptPDF(createPDFInvoiceDO, "Email")
            return

        } else {
            //Display error message
        }
    }
}