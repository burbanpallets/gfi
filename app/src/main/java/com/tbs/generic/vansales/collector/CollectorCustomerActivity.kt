package com.tbs.generic.vansales.collector

import android.content.Intent
import android.os.Build
import androidx.annotation.RequiresApi
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tbs.generic.vansales.Activitys.BaseActivity
import com.tbs.generic.vansales.Model.CustomerDo
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.pdfs.utils.PDFConstants
import com.tbs.generic.vansales.utils.PreferenceUtils
import com.tbs.generic.vansales.utils.Util
import kotlinx.android.synthetic.main.configuration_layout.*

class CollectorCustomerActivity : BaseActivity() {
    lateinit var userId: String
    lateinit var orderCode: String
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var customerAdapter: CollectorCustomerAdapter
    lateinit var llOrderHistory: View
    lateinit var customerDos: ArrayList<CustomerDo>
    private lateinit var btnAddMore: Button
    lateinit var llSearch: LinearLayout
    lateinit var etSearch: EditText
    lateinit var ivClearSearch: ImageView
    lateinit var ivGoBack: ImageView
    lateinit var ivSearchs: ImageView
    lateinit var ivMenuu: ImageView
    lateinit var ivRefreshh: ImageView
    lateinit var ivPay: ImageView
    lateinit var tvNoOrders: TextView
    private lateinit var tvScreenTitles: TextView

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    override fun initialize() {
        llOrderHistory = layoutInflater.inflate(R.layout.credit_customer_screen, null)
        llBody.addView(llOrderHistory, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        /* flToolbar.visibility = View.VISIBLE
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            var role = preferenceUtils.getIntFromPreference(PreferenceUtils.USER_ROLE, 0)
            if (role == 10) {
                finish()

            } else if (role == 30) {
                finishAffinity()

            } else {
                finish()
            }
//            overridePendingTransition(R.anim.exit, R.anim.enter)
        }*/

        flToolbar.visibility = View.GONE

        setSupportActionBar(act_toolbar)
        Util.getActionBarView(supportActionBar)

        act_toolbar.setNavigationOnClickListener {
            var role = preferenceUtils.getIntFromPreference(PreferenceUtils.USER_ROLE, 0)
            if (role == 10) {
                finish()

            } else if (role == 30) {
                finishAffinity()

            } else {
                finish()
            }
        }
        initializeControls()
        ivSelectAll.visibility = View.GONE
        ivSelectAll.tag = false
        ivSelectAll.setOnClickListener {
            if (customerDos != null && customerDos.size > 0) {
                if (ivSelectAll.tag as Boolean) {
                    ivSelectAll.tag = false
                    ivSelectAll.setImageResource(R.drawable.select_all)// change image as checked
                    if (customerDos != null && customerDos.size > 0) {
                        for (i in customerDos.indices) {
                            customerDos.get(i).isSelected = false
                        }
                        customerAdapter = CollectorCustomerAdapter(this@CollectorCustomerActivity, customerDos, "CREDIT")
                        recycleview.adapter = customerAdapter

//                    recycleview.smoothScrollToPosition(invoiceAdapter.getItemCount() - 1);
                    }
                } else {
                    ivSelectAll.tag = true
                    ivSelectAll.setImageResource(R.drawable.selected)
//                change image like unchecked
                    if (customerDos != null && customerDos.size > 0) {
                        for (i in customerDos.indices) {
                            customerDos.get(i).isSelected = true
                        }
                        customerAdapter = CollectorCustomerAdapter(this@CollectorCustomerActivity, customerDos, "CREDIT")
                        recycleview.adapter = customerAdapter

//                    recycleview.smoothScrollToPosition(invoiceAdapter.getItemCount() - 1);

                    }
                }
            }
        }
        tv_title.text = getString(R.string.customer_list)
        tvScreenTitles.visibility = View.VISIBLE
        ivGoBack.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                finishAffinity()
            }
        }
        var role = preferenceUtils.getIntFromPreference(PreferenceUtils.USER_ROLE, 0)
        if (role == 10) {
            ivMenu.visibility = View.GONE

        } else if (role == 30) {
            ivMenu.visibility = View.VISIBLE

        } else {
            ivMenu.visibility = View.GONE
        }
        customerAdapter = CollectorCustomerAdapter(this@CollectorCustomerActivity, ArrayList(), "CREDIT")

        btnAddMore.setOnClickListener {

            if (customerAdapter != null) {
                val selectedCustomerDOs = customerAdapter.getselectedCustomerDOs()
                if (selectedCustomerDOs != null && !selectedCustomerDOs.isEmpty()) {
                    val intent = Intent(this@CollectorCustomerActivity, InvoiceListActivity::class.java)
                    intent.putExtra("selectedCustomerDOs", selectedCustomerDOs)
                    startActivityForResult(intent, 11)
                } else {
                    showToast("Please select Customers")
                }
            } else {
                showToast("No Customers found")
            }

        }

        ivMenu.setOnClickListener {
            //
            val popup = PopupMenu(this@CollectorCustomerActivity, ivMenu)
            popup.menuInflater.inflate(R.menu.credit_menu, popup.menu)
            popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                override fun onMenuItemClick(item: MenuItem): Boolean {
                    if (item.title.equals("Sync")) {
                        val driverListRequest = CollectorCustomerNewRequest(this@CollectorCustomerActivity)
                        driverListRequest.setOnResultListener { isError, customerDOs ->
                            hideLoader()
                            if (isError) {
                                Toast.makeText(this@CollectorCustomerActivity, R.string.error_customer_list, Toast.LENGTH_SHORT).show()
                            } else {
                                customerDos = customerDOs
                                if (customerDos != null && customerDos.size > 0) {
                                    StorageManager.getInstance(this@CollectorCustomerActivity).saveSpotSalesCustomerList(this@CollectorCustomerActivity, customerDos)

                                    customerAdapter = CollectorCustomerAdapter(this@CollectorCustomerActivity, customerDos, "CREDIT")
                                    recycleview.adapter = customerAdapter
                                    tvNoOrders.visibility = View.GONE
                                    recycleview.visibility = View.VISIBLE
                                } else {
                                    tvNoOrders.visibility = View.VISIBLE
                                    recycleview.visibility = View.GONE
                                    showToast("No customers")
                                }
                            }
                        }
                        driverListRequest.execute()


                    } else if (item.title.equals("Activity Report")) {
                        val intent = Intent(this@CollectorCustomerActivity, TransactionListActivity::class.java)

                        startActivity(intent)

                    } else if (item.title.equals("Logout")) {
                        logOut()


                    } else if (item.title.equals("Document Preview")) {
                        val id = preferenceUtils.getStringFromPreference(PreferenceUtils.PAYMENT_ID, "")
                        if (id.length > 0) {
                            val intent = Intent(applicationContext, CollectorPreviewActivity::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                            intent.putExtra("KEY", PDFConstants.RECEIPT_PDF_NAME)
                            intent.putExtra("COLLECTOR", "Collector")
                            startActivity(intent)
                        } else {
                            showToast("Please create payment for preview")
                        }


                    }

                    return true

                }


            })
            popup.show()//showing popup menu

        }
        ivSearchs.setOnClickListener {
            //            if(llSearch.visibility == View.VISIBLE){
//                llSearch.visibility = View.INVISIBLE
//            }
//            else{
//            }
            tvScreenTitles.visibility = View.GONE
            llSearch.visibility = View.VISIBLE
        }
        ivClearSearch.setOnClickListener {
            etSearch.setText("")
            if (customerDos != null && customerDos.size > 0) {
                customerAdapter = CollectorCustomerAdapter(this@CollectorCustomerActivity, customerDos, "CREDIT")
//                recycleview.adapter = customerAdapter
                customerAdapter.refreshAdapter(customerDos)

                tvNoOrders.visibility = View.GONE
                recycleview.visibility = View.VISIBLE
            } else {
                tvNoOrders.visibility = View.VISIBLE
                recycleview.visibility = View.GONE
            }
        }

        etSearch.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(editable: Editable?) {
                if (etSearch.text.toString().equals("", true)) {
                    if (customerDos != null && customerDos.size > 0) {
//                        customerAdapter = CollectorCustomerAdapter(this@CollectorCustomerActivity, customerDos, "CREDIT")
//                        recycleview.adapter=customerAdapter
                        customerAdapter.refreshAdapter(customerDos)
                        tvNoOrders.visibility = View.GONE
                        recycleview.visibility = View.VISIBLE
                    } else {
                        tvNoOrders.visibility = View.VISIBLE
                        recycleview.visibility = View.GONE
                    }
                } else if (etSearch.text.toString().length > 2) {
                    filter(etSearch.text.toString())
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })
//        customerDos = StorageManager.getInstance(this).getSpotSalesCustomerList(this)
//        if (customerDos != null && customerDos.size > 0) {
//            showLoader()
//            customerAdapter = CollectorCustomerAdapter(this@CollectorCustomerActivity, customerDos, "CREDIT")
//            recycleview.adapter = customerAdapter
//            recycleview.visibility = View.VISIBLE
//            tvNoOrders.visibility = View.GONE
//            hideLoader()
//        } else {
//
//            val driverListRequest = CollectorCustomerNewRequest(this@CollectorCustomerActivity)
//
//            driverListRequest.setOnResultListener { isError, customerDOs ->
//                hideLoader()
//                if (isError) {
//                    Toast.makeText(this@CollectorCustomerActivity, R.string.error_customer_list, Toast.LENGTH_SHORT).show()
//                } else {
//                    hideLoader()
//
//                    customerDos = customerDOs
//                    if (customerDos != null && customerDos.size > 0) {
//                        StorageManager.getInstance(this).saveSpotSalesCustomerList(this, customerDos);
//
//                        customerAdapter = CollectorCustomerAdapter(this@CollectorCustomerActivity, customerDos, "CREDIT")
//                        recycleview!!.adapter = customerAdapter
//                        tvNoOrders.visibility = View.GONE
//                        recycleview.visibility = View.VISIBLE
//                    } else {
//                        tvNoOrders.visibility = View.VISIBLE
//                        recycleview.visibility = View.GONE
//                        showToast("No customers")
//                    }
//                }
//            }
//            driverListRequest.execute()
//        }
    }

    override fun initializeControls() {
        recycleview = findViewById<View>(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        llSearch = findViewById<View>(R.id.llSearch) as LinearLayout
        etSearch = findViewById<View>(R.id.etSearch) as EditText
        ivClearSearch = findViewById<View>(R.id.ivClearSearch) as ImageView
        ivGoBack = findViewById<View>(R.id.ivGoBack) as ImageView
        ivSearchs = findViewById<View>(R.id.ivSearchs) as ImageView
        tvNoOrders = findViewById<View>(R.id.tvNoOrders) as TextView
        tvScreenTitles = findViewById<View>(R.id.tvScreenTitles) as TextView
        ivPay = findViewById<View>(R.id.ivPay) as ImageView
        ivPay.setOnClickListener {
            val intent = Intent(this@CollectorCustomerActivity, SelectTransactionListActivity::class.java)
            startActivity(intent)
        }
        btnAddMore = findViewById<Button>(R.id.btnAddMore)

        recycleview.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this@CollectorCustomerActivity, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
        ivMenuu = findViewById<View>(R.id.ivMenuu) as ImageView
        ivRefreshh = findViewById<View>(R.id.ivRefreshh) as ImageView

    }

    private fun filter(filtered: String): ArrayList<CustomerDo> {
        val customerDOs = ArrayList<CustomerDo>()
        for (i in customerDos.indices) {
            if (customerDos.get(i).customerName.contains(filtered, true)
                    || customerDos.get(i).customer.contains(filtered, true)) {
                customerDOs.add(customerDos.get(i))
            }
        }
        if (customerDOs.size > 0) {
            customerAdapter.refreshAdapter(customerDOs)
            tvNoOrders.visibility = View.GONE
            recycleview.visibility = View.VISIBLE
        } else {
            tvNoOrders.visibility = View.VISIBLE
            recycleview.visibility = View.GONE
        }
        return customerDOs
    }

    override fun onBackPressed() {
        if (!svSearch.isIconified) {
            svSearch.isIconified = true
            return
        }
        var role = preferenceUtils.getIntFromPreference(PreferenceUtils.USER_ROLE, 0)
        if (role == 10) {
            finish()

        } else if (role == 30) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                finishAffinity()
            }

        } else {
            finish()
        }
        super.onBackPressed()
    }

    override fun onResume() {
        val driverListRequest = CollectorCustomerNewRequest(this@CollectorCustomerActivity)

        driverListRequest.setOnResultListener { isError, customerDOs ->
            hideLoader()
            if (isError) {
                Toast.makeText(this@CollectorCustomerActivity, R.string.error_customer_list, Toast.LENGTH_SHORT).show()
            } else {
                customerDos = customerDOs
                if (customerDos != null && customerDos.size > 0) {
                    StorageManager.getInstance(this@CollectorCustomerActivity).saveSpotSalesCustomerList(this@CollectorCustomerActivity, customerDos)

                    customerAdapter = CollectorCustomerAdapter(this@CollectorCustomerActivity, customerDos, "CREDIT")
                    recycleview.adapter = customerAdapter
                    tvNoOrders.visibility = View.GONE
                    recycleview.visibility = View.VISIBLE
                } else {
                    tvNoOrders.visibility = View.VISIBLE
                    recycleview.visibility = View.GONE
                    showToast("No Customers")
                }
            }
        }
        driverListRequest.execute()
        super.onResume()
    }
}
