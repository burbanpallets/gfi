package com.tbs.generic.vansales.collector

import android.content.Intent
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tbs.generic.vansales.Activitys.BaseActivity
import com.tbs.generic.vansales.Model.CustomerDo
import com.tbs.generic.vansales.Model.LoadStockDO
import com.tbs.generic.vansales.Model.UnPaidInvoiceMainDO
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.UnPaidInvoicesListRequest
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.utils.PreferenceUtils
import java.util.*


class InvoiceListActivity : BaseActivity() {
    private var invoiceAdapter: com.tbs.generic.vansales.collector.CreatePaymentAdapter = com.tbs.generic.vansales.collector.CreatePaymentAdapter(this, ArrayList(), "", "")
    lateinit var loadStockDOs: ArrayList<LoadStockDO>
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var linearLayoutManager: androidx.recyclerview.widget.LinearLayoutManager

    lateinit var tvNoDataFound: TextView
    lateinit var tvPaidAmount: TextView
    private var unPaidInvoiceMainDO: UnPaidInvoiceMainDO = UnPaidInvoiceMainDO()
    lateinit var view: LinearLayout
    private var fromId = 0
    private lateinit var siteListRequest: UnPaidInvoicesListRequest
    private var from: String = ""
    private var customerid = ""
    private lateinit var btnAddMore: Button
    private var selectedCustomerDOs = ArrayList<CustomerDo>()

    override fun onResume() {
        super.onResume()
//        selectInvoiceList()
    }

    override fun initialize() {
        var llCategories = layoutInflater.inflate(R.layout.credit_invoice_list, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        ivSelectAll.visibility = View.VISIBLE
        changeLocale()
        initializeControls()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
        ivSelectAll.tag = false
        ivSelectAll.setOnClickListener {
            if (unPaidInvoiceMainDO != null && unPaidInvoiceMainDO.unPaidInvoiceDOS != null && unPaidInvoiceMainDO.unPaidInvoiceDOS.size > 0) {
                if (ivSelectAll.tag as Boolean) {
                    ivSelectAll.tag = false
                    ivSelectAll.setImageResource(R.drawable.select_all)// change image as checked
                    if (unPaidInvoiceMainDO != null && unPaidInvoiceMainDO.unPaidInvoiceDOS != null && unPaidInvoiceMainDO.unPaidInvoiceDOS.size > 0) {
                        for (i in unPaidInvoiceMainDO.unPaidInvoiceDOS.indices) {
                            unPaidInvoiceMainDO.unPaidInvoiceDOS.get(i).isSelected = false
                        }
                        invoiceAdapter = com.tbs.generic.vansales.collector.CreatePaymentAdapter(this@InvoiceListActivity, unPaidInvoiceMainDO.unPaidInvoiceDOS, customerid, from)
                        recycleview.adapter = invoiceAdapter
//                    recycleview.smoothScrollToPosition(invoiceAdapter.getItemCount() - 1);
                    }
                } else {
                    ivSelectAll.tag = true
                    ivSelectAll.setImageResource(R.drawable.selected)
//                change image like unchecked
                    if (unPaidInvoiceMainDO != null && unPaidInvoiceMainDO.unPaidInvoiceDOS != null && unPaidInvoiceMainDO.unPaidInvoiceDOS.size > 0) {
                        for (i in unPaidInvoiceMainDO.unPaidInvoiceDOS.indices) {
                            unPaidInvoiceMainDO.unPaidInvoiceDOS.get(i).isSelected = true
                        }
                        invoiceAdapter = com.tbs.generic.vansales.collector.CreatePaymentAdapter(this@InvoiceListActivity, unPaidInvoiceMainDO.unPaidInvoiceDOS, customerid, from)
                        recycleview.adapter = invoiceAdapter
//                    recycleview.smoothScrollToPosition(invoiceAdapter.getItemCount() - 1);

                    }
                }
            }
        }

    }

    override fun initializeControls() {
        tvScreenTitle.text = "Select Invoice"
        recycleview = findViewById<androidx.recyclerview.widget.RecyclerView>(R.id.recycleview)
        linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        tvNoDataFound = findViewById<TextView>(R.id.tvNoDataFound)
        view = findViewById<LinearLayout>(R.id.view)
        btnAddMore = findViewById<Button>(R.id.btnAddMore)

        if (intent.hasExtra("selectedCustomerDOs")) {
            selectedCustomerDOs = intent.getSerializableExtra("selectedCustomerDOs") as ArrayList<CustomerDo>
        }
        recycleview.layoutManager = linearLayoutManager

        selectInvoiceList()
        if (intent.hasExtra("CODE")) {
            customerid = intent.extras?.getString("CODE")!!
        }
        val btnCreate = findViewById<Button>(R.id.btnCreate)

        btnCreate.setOnClickListener {
            if (invoiceAdapter != null) {
                val selectedUnPaidInvoiceDOS = invoiceAdapter.getselectedUnPaidInvoiceDOS()
                if (selectedUnPaidInvoiceDOS != null && !selectedUnPaidInvoiceDOS.isEmpty()) {

                    val intent = Intent(this@InvoiceListActivity, com.tbs.generic.vansales.collector.CreatePaymentActivity::class.java)
                    intent.putExtra("selectedInvoiceDOs", selectedUnPaidInvoiceDOS)
                    intent.putExtra("Sales", 1)
                    intent.putExtra("CODE", "")
                    intent.putExtra("selectedCustomerDOs", selectedCustomerDOs)

                    startActivityForResult(intent, 11)
                } else {
                    if (selectedCustomerDOs.size > 1) {
                        showToast(getString(R.string.please_select_inovice))

                    } else {
                        val intent = Intent(this@InvoiceListActivity, com.tbs.generic.vansales.collector.CreatePaymentActivity::class.java)
                        intent.putExtra("Sales", 1)
                        intent.putExtra("CODE", "")
                        intent.putExtra("selectedCustomerDOs", selectedCustomerDOs)

                        startActivityForResult(intent, 11)
                    }
                }
            } else {
                val intent = Intent(this@InvoiceListActivity, com.tbs.generic.vansales.collector.CreatePaymentActivity::class.java)
                intent.putExtra("Sales", 1)
                intent.putExtra("CODE", "")
                intent.putExtra("selectedCustomerDOs", selectedCustomerDOs)

                startActivityForResult(intent, 11); }
        }
        btnAddMore.setOnClickListener {

            if (invoiceAdapter != null) {
                val selectedUnPaidInvoiceDOS = invoiceAdapter.getselectedUnPaidInvoiceDOS()
                if (selectedUnPaidInvoiceDOS != null && !selectedUnPaidInvoiceDOS.isEmpty()) {
                    val intent = Intent(this@InvoiceListActivity, com.tbs.generic.vansales.collector.CreatePaymentActivity::class.java)
                    intent.putExtra("selectedInvoiceDOs", selectedUnPaidInvoiceDOS)
                    intent.putExtra("Sales", 1)
                    intent.putExtra("CODE", "")
                    intent.putExtra("selectedCustomerDOs", selectedCustomerDOs)

                    startActivityForResult(intent, 11)
                } else {
                    showToast(getString(R.string.please_select_inovice))
                }
            } else {
                showToast(getString(R.string.no_invoices_found))
            }

        }
    }


    private fun selectInvoiceList() {


//        var tvInvoiceId = findViewById(R.id.tvInvoiceId) as TextView
        var tvNoData = findViewById<TextView>(R.id.tvNoDataFound)
        var tvAmount = findViewById<TextView>(R.id.tvAmount)
        tvPaidAmount = findViewById<TextView>(R.id.tvPaidAmount)


        recycleview.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        val activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)
        val customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)

        if (intent.hasExtra("CODE")) {
            customerid = intent.extras?.getString("CODE").toString()
        }

        val ShipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, resources.getString(R.string.checkin_non_scheduled))



        if (selectedCustomerDOs != null && selectedCustomerDOs.size > 0) {
            var multisiteListRequest = CrediUnPaidInvoicesListRequest(selectedCustomerDOs, this@InvoiceListActivity)

            multisiteListRequest.setOnResultListener { isError, unPaidInvoiceMainDo ->
                hideLoader()
                if (unPaidInvoiceMainDo != null && unPaidInvoiceMainDo.unPaidInvoiceDOS.size > 0) {
                    unPaidInvoiceMainDO = unPaidInvoiceMainDo
//                    tvInvoiceId.setVisibility(View.VISIBLE)
                    tvAmount.visibility = View.VISIBLE
                    tvAmount.text = getString(R.string.total_amount) + " : " + String.format("%.2f", unPaidInvoiceMainDo.totalAmount) + " " + unPaidInvoiceMainDo.unPaidInvoiceDOS.get(0).currency
                    tvPaidAmount.visibility = View.VISIBLE

                    preferenceUtils.saveString(PreferenceUtils.TOTAL_AMOUNT, String.format("%.2f", unPaidInvoiceMainDo.totalAmount))
                    preferenceUtils.saveInt(PreferenceUtils.PAYMENT_TYPE, unPaidInvoiceMainDo.type)


                    tvNoData.visibility = View.GONE
                    if (isError) {
                        hideLoader()

                        Toast.makeText(this@InvoiceListActivity, resources.getString(R.string.error_NoData), Toast.LENGTH_SHORT).show()
                    } else {
                        hideLoader()
                        tvPaidAmount.visibility = View.VISIBLE

                        tvAmount.visibility = View.VISIBLE
                        view.visibility = View.VISIBLE
                        this.from = "CREDIT"
                        invoiceAdapter = com.tbs.generic.vansales.collector.CreatePaymentAdapter(this@InvoiceListActivity, unPaidInvoiceMainDo.unPaidInvoiceDOS, customerid, "CREDIT")
                        recycleview.adapter = invoiceAdapter
                        tvAmount.text = getString(R.string.total_amount) + " : " + String.format("%.2f", unPaidInvoiceMainDo.totalAmount) + " " + unPaidInvoiceMainDo.unPaidInvoiceDOS.get(0).currency
                        preferenceUtils.saveString(PreferenceUtils.TOTAL_AMOUNT, "" + String.format("%.2f", unPaidInvoiceMainDo.totalAmount))

//                    var siteAdapter = CreatePaymentAdapter(this@CreatePaymentActivity, createPaymentDo)
//                    recyclerView.setAdapter(siteAdapter)f

                    }
                } else {
//                tvInvoiceId.setVisibility(View.VISIBLE)
                    tvAmount.visibility = View.GONE
                    tvPaidAmount.visibility = View.GONE

                    tvNoData.visibility = View.VISIBLE
                    recycleview.visibility = View.GONE
                    view.visibility = View.VISIBLE

                    hideLoader()

                }

            }

            multisiteListRequest.execute()
        } else {
            var siteListRequest = UnPaidInvoicesListRequest(customerid, this@InvoiceListActivity)

            siteListRequest.setOnResultListener { isError, unPaidInvoiceMainDo ->
                hideLoader()
                this.unPaidInvoiceMainDO = unPaidInvoiceMainDo

                if (unPaidInvoiceMainDO != null && unPaidInvoiceMainDo.unPaidInvoiceDOS.size > 0) {
                    this.unPaidInvoiceMainDO = unPaidInvoiceMainDo
//                    tvInvoiceId.setVisibility(View.VISIBLE)
                    tvAmount.visibility = View.VISIBLE
                    tvAmount.text = getString(R.string.total_amount) + " : " + String.format("%.2f", unPaidInvoiceMainDo.totalAmount) + " " + unPaidInvoiceMainDo.unPaidInvoiceDOS.get(0).currency

                    preferenceUtils.saveString(PreferenceUtils.TOTAL_AMOUNT, String.format("%.2f", unPaidInvoiceMainDo.totalAmount))
                    preferenceUtils.saveInt(PreferenceUtils.PAYMENT_TYPE, unPaidInvoiceMainDo.type)


                    tvNoData.visibility = View.GONE
                    if (isError) {
                        hideLoader()

                        Toast.makeText(this@InvoiceListActivity, resources.getString(R.string.error_NoData), Toast.LENGTH_SHORT).show()
                    } else {
                        hideLoader()

                        tvAmount.visibility = View.VISIBLE
                        view.visibility = View.VISIBLE
                        this.from = ""
                        invoiceAdapter = com.tbs.generic.vansales.collector.CreatePaymentAdapter(this@InvoiceListActivity, unPaidInvoiceMainDo.unPaidInvoiceDOS, customerid, "")
                        recycleview.adapter = invoiceAdapter
                        tvAmount.text = getString(R.string.total_amount) + " : " + String.format("%.2f", unPaidInvoiceMainDo.totalAmount) + " " + unPaidInvoiceMainDo.unPaidInvoiceDOS.get(0).currency
                        preferenceUtils.saveString(PreferenceUtils.TOTAL_AMOUNT, "" + String.format("%.2f", unPaidInvoiceMainDo.totalAmount))

//                    var siteAdapter = CreatePaymentAdapter(this@CreatePaymentActivity, createPaymentDo)
//                    recyclerView.setAdapter(siteAdapter)f

                    }
                } else {
//                tvInvoiceId.setVisibility(View.VISIBLE)
                    tvAmount.visibility = View.GONE
                    tvPaidAmount.visibility = View.GONE

                    tvNoData.visibility = View.VISIBLE
                    recycleview.visibility = View.GONE
                    view.visibility = View.VISIBLE

                    hideLoader()

                }

            }

            siteListRequest.execute()
        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 11 && resultCode == 1) {
            selectInvoiceList()
        }
    }


}