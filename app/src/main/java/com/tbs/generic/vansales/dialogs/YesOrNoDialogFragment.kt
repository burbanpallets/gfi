package com.tbs.generic.vansales.dialogs

import android.R.style.Theme_Holo_Light_Dialog_NoActionBar_MinWidth
import android.app.Dialog
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.annotation.Nullable
import androidx.fragment.app.DialogFragment
import android.text.TextUtils
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.listeners.ResultListner
import kotlinx.android.synthetic.main.dialog_yes_or_no.*

class YesOrNoDialogFragment : DialogFragment() {
    private lateinit var resultListner: ResultListner
    private lateinit var message: String
    private lateinit var title: String
    private var showCancle: Boolean = false


    fun newInstance(title: String,
                    message: String,
                    resultListner: ResultListner,
                    showCancle: Boolean): YesOrNoDialogFragment {
        val yesOrNoDialogFragment = YesOrNoDialogFragment()
        yesOrNoDialogFragment.message = message
        yesOrNoDialogFragment.resultListner = resultListner
        yesOrNoDialogFragment.title = title
        yesOrNoDialogFragment.showCancle = showCancle
        return yesOrNoDialogFragment
    }

    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NO_TITLE, Theme_Holo_Light_Dialog_NoActionBar_MinWidth)

    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.window?.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)

        return dialog
    }


    @Nullable
    override fun onCreateView(inflater: LayoutInflater, @Nullable container: ViewGroup?, @Nullable savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.dialog_yes_or_no, null)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (!TextUtils.isEmpty(message)) {
            tv_message.text = message
        }

        if (!TextUtils.isEmpty(title)) {
            tv_title.text = title
        }

        if (!showCancle) {
            btn_no.visibility = View.GONE
            //ll_buttons.gravity = Gravity.CENTER
        } else {
            //ll_buttons.gravity = Gravity.END
        }
        btn_yes.setOnClickListener {
            resultListner.onResultListner(true, true)

            dismiss()
        }
        btn_no.setOnClickListener {
            resultListner.onResultListner(false, false)
            dismiss()
        }

    }
}