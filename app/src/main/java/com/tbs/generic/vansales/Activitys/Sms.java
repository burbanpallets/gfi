package com.tbs.generic.vansales.Activitys;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.tbs.generic.vansales.Model.TestDO;
import com.tbs.generic.vansales.Model.TestMainDO;
import com.tbs.generic.vansales.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class Sms extends AppCompatActivity {
    ProgressDialog pd;
    TextView tvResult;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms);
        tvResult=findViewById(R.id.tvResult);
//        sendSmsMsgFnc("9493998127","hi");

        new JsonTask().execute("https://samples.openweathermap.org/data/2.5/weather?zip=93709, us&appid=b6907d289e10d714a6e88b30761fae22");
    }
    void sendSmsMsgFnc(String mblNumVar, String smsMsgVar)
    {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED)
        {
            try
            {
                SmsManager smsMgrVar = SmsManager.getDefault();
                smsMgrVar.sendTextMessage(mblNumVar, null, smsMsgVar, null, null);
                Toast.makeText(getApplicationContext(), "Message Sent",
                        Toast.LENGTH_LONG).show();
            }
            catch (Exception ErrVar)
            {
                Toast.makeText(getApplicationContext(),ErrVar.getMessage().toString(),
                        Toast.LENGTH_LONG).show();
                ErrVar.printStackTrace();
            }
        }
        else
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            {
                requestPermissions(new String[]{Manifest.permission.SEND_SMS}, 10);
            }
        }

    }



    private class JsonTask extends AsyncTask<String, String, String> {

        protected void onPreExecute() {
            super.onPreExecute();

            pd = new ProgressDialog(Sms.this);
            pd.setMessage("Please wait");
            pd.setCancelable(false);
            pd.show();
        }

        protected String doInBackground(String... params) {


            HttpURLConnection connection = null;
            BufferedReader reader = null;

            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();


                InputStream stream = connection.getInputStream();

                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();
                String line = "";

                while ((line = reader.readLine()) != null) {
                    buffer.append(line+"\n");
                    Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)

                }

                return buffer.toString();


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            pd.hide();
            try {
                TestMainDO testMainDO = new TestMainDO();
                JSONObject  obj = new JSONObject(result);
                testMainDO.name=obj.optString("name");
                JSONObject coord=obj.optJSONObject("coord");

//                    TestDO testDO= new TestDO();
                testMainDO.lat=coord.optDouble("lat");
                testMainDO.lon=coord.optDouble("lon");
//                    testMainDO.testDOS.add(testDO);
                JSONObject sys=obj.optJSONObject("sys");
                testMainDO.type=sys.optString("type");
                testMainDO.id=sys.optString("id");
                testMainDO.message=sys.optString("message");
                testMainDO.country=sys.optString("country");
                testMainDO.sunrise=sys.optString("sunrise");
                testMainDO.sunset=sys.optString("sunset");

                JSONObject main=obj.optJSONObject("main");
                testMainDO.temp=main.optString("temp");
                testMainDO.humidity=main.optString("humidity");
                testMainDO.pressure=main.optString("pressure");
                testMainDO.temp_min=main.optString("temp_min");
                testMainDO.temp_max=main.optString("temp_max");

                tvResult.setText("Longitute - "+testMainDO.lon+"\n"+
                        "Lattitude - "+testMainDO.lat+"\n"+
                        "Humidity - "+testMainDO.humidity+"\n"+
                        "Name - "+testMainDO.name+"\n"+
                        "Zipcode - "+"93709");

            } catch (JSONException e) {
                e.printStackTrace();
            }

//            tvResult.setText(""+result);
        }
    }
}