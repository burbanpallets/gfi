package com.tbs.generic.vansales.Activitys

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.media.AudioManager
import android.media.ToneGenerator
import android.os.Bundle
import android.view.SurfaceHolder
import android.view.SurfaceView
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import com.google.android.gms.vision.CameraSource
import com.google.android.gms.vision.Detector
import com.google.android.gms.vision.Detector.Detections
import com.google.android.gms.vision.barcode.Barcode
import com.google.android.gms.vision.barcode.BarcodeDetector
import com.tbs.generic.vansales.Activitys.TestBarcodeActivity
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.common.AppConstants
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.utils.Util
import kotlinx.android.synthetic.main.barcode_test.*
import kotlinx.android.synthetic.main.configuration_layout.*
import kotlinx.android.synthetic.main.include_toolbar.*
import kotlinx.android.synthetic.main.include_toolbar.act_toolbar
import kotlinx.android.synthetic.main.include_toolbar.tv_title
import java.io.IOException

class TestBarcodeActivity : BaseActivity() {
    private var surfaceView: SurfaceView? = null
    private var barcodeDetector: BarcodeDetector? = null
    private var cameraSource: CameraSource? = null
    private var toneGen1: ToneGenerator? = null
    private var barcodeText: TextView? = null
    private var barcodeData: String? = null


    private fun initialiseDetectorsAndSources() {


        //Toast.makeText(getApplicationContext(), "Barcode scanner started", Toast.LENGTH_SHORT).show();
        barcodeDetector = BarcodeDetector.Builder(this)
                .setBarcodeFormats(Barcode.ALL_FORMATS)
                .build()
        btn_save.setOnClickListener {
            if (!barcodeText!!.text.toString().isNullOrEmpty()) {
                var boolean:Boolean=false
                var stockDos = StorageManager.getInstance(this).getSerialList(this)
                for (i in stockDos.indices) {
                    if (barcodeText!!.text.toString().equals(stockDos.get(i).serialDO)) {
                        boolean=true
                        break
                    } else {
                        boolean=false
                    }

                }
                if(boolean==false){
                    val intent = Intent()

                    intent.putExtra("SERIAL", barcodeText!!.text.toString())
                    setResult(11, intent)
                    finish()
                }else{
                    showToast(getString(R.string.duplicateserials))
                }


            } else {
                showToast(getString(R.string.scancode))
            }


        }
        cameraSource = CameraSource.Builder(this, barcodeDetector)
                .setRequestedPreviewSize(1920, 1080)
                .setAutoFocusEnabled(true) //you should add this feature
                .build()
        surfaceView!!.holder.addCallback(object : SurfaceHolder.Callback {
            override fun surfaceCreated(holder: SurfaceHolder) {
                try {
                    if (ActivityCompat.checkSelfPermission(this@TestBarcodeActivity, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        cameraSource!!.start(surfaceView!!.holder)
                    } else {
                        ActivityCompat.requestPermissions(this@TestBarcodeActivity, arrayOf(Manifest.permission.CAMERA), REQUEST_CAMERA_PERMISSION)
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }

            override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {}
            override fun surfaceDestroyed(holder: SurfaceHolder) {
                cameraSource!!.stop()
            }
        })

        barcodeDetector!!.setProcessor(object : Detector.Processor<Barcode> {
            override fun release() {
                // Toast.makeText(getApplicationContext(), "To prevent memory leaks barcode scanner has been stopped", Toast.LENGTH_SHORT).show();
            }

            override fun receiveDetections(detections: Detections<Barcode>) {
                val barcodes = detections.detectedItems
                if (barcodes.size() != 0) {
                    barcodeText!!.post {
                        if (barcodes.valueAt(0).email != null) {
                            barcodeText!!.removeCallbacks(null)
                            barcodeData = barcodes.valueAt(0).email.address
                            barcodeText!!.text = barcodeData
                            toneGen1!!.startTone(ToneGenerator.TONE_CDMA_PIP, 150)
                        } else {
                            barcodeData = barcodes.valueAt(0).displayValue
                            barcodeText!!.text = barcodeData
                            toneGen1!!.startTone(ToneGenerator.TONE_CDMA_PIP, 150)
                        }
                    }
                }
            }
        })
    }

    override fun onPause() {
        super.onPause()
//        supportActionBar!!.hide()
        cameraSource!!.release()
    }

    override fun onResume() {
        super.onResume()
//        supportActionBar!!.hide()
        initialiseDetectorsAndSources()
    }

    override fun initialize() {
        val llCategories = layoutInflater.inflate(R.layout.barcode_test, null) as LinearLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        disableMenuWithBackButton()
        changeLocale()

        initializeControls()

        flToolbar.visibility = View.GONE

        setSupportActionBar(act_toolbar)
        Util.getActionBarView(supportActionBar)
        act_toolbar.setNavigationOnClickListener {
            finish()
        }
    }

    override fun initializeControls() {
        tv_title.setText(resources.getString(R.string.scan_bar))

        toneGen1 = ToneGenerator(AudioManager.STREAM_MUSIC, 100)
        surfaceView = findViewById(R.id.surface_view)
        barcodeText = findViewById(R.id.barcode_text)
        initialiseDetectorsAndSources()
    }

    companion object {
        private const val REQUEST_CAMERA_PERMISSION = 201
    }
}