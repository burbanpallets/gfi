//package com.tbs.generic.vansales.Activitys
//
//import android.content.Intent
//import androidx.recyclerview.widget.LinearLayoutManager
//import androidx.recyclerview.widget.RecyclerView
//import android.view.View
//import android.view.ViewGroup
//import android.widget.*
//import com.tbs.generic.vansales.Adapters.InvoiceAdapter
//import com.tbs.generic.vansales.Model.LoadStockDO
//import com.tbs.generic.vansales.R
//import com.tbs.generic.vansales.Requests.InvoiceHistoryRequest
//import com.tbs.generic.vansales.utils.Util
//import java.util.ArrayList
//
//
//class SalesInvoiceActivity : BaseActivity() {
//    lateinit var invoiceAdapter: InvoiceAdapter
//    lateinit var loadStockDOs: ArrayList<LoadStockDO>
//    lateinit var recycleview : androidx.recyclerview.widget.RecyclerView
//    lateinit var tvNoDataFound:TextView
//    var fromId= 0
//
//    override fun onResume() {
//        super.onResume()
//    }
//    override fun initialize() {
//      var  llCategories = layoutInflater.inflate(R.layout.sales_invoice_list, null) as RelativeLayout
//        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
//        changeLocale()
//        initializeControls()
//
//        toolbar.setNavigationIcon(R.drawable.back)
//        toolbar.setNavigationOnClickListener { finish() }
//    }
//    override fun initializeControls() {
//        tvScreenTitle.setText(R.string.sales_invoice)
//        recycleview = findViewById<androidx.recyclerview.widget.RecyclerView>(R.id.recycleview)
//        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
//        tvNoDataFound = findViewById<TextView>(R.id.tvNoDataFound)
//        if (intent.hasExtra("Sales")) {
//            fromId = intent.extras!!.getInt("Sales")
//        }
//        recycleview.layoutManager = linearLayoutManager
//
////        invoiceAdapter = InvoiceAdapter(this@SalesInvoiceActivity,  null)
////        recycleview.setAdapter(invoiceAdapter)
////
//
//        if (Util.isNetworkAvailable(this)) {
//
//            val driverListRequest = InvoiceHistoryRequest("",this@SalesInvoiceActivity)
//            driverListRequest.setOnResultListener { isError, invoiceHistoryMainDO ->
//                hideLoader()
//                if (isError) {
//                    tvNoDataFound.visibility = View.VISIBLE
//                    recycleview.visibility = View.GONE
//                    Toast.makeText(this@SalesInvoiceActivity, R.string.error_NoData, Toast.LENGTH_SHORT).show()
//                } else {
//
//                    if(invoiceHistoryMainDO.invoiceHistoryDOS.size>0){
//                        tvNoDataFound.visibility = View.GONE
//                        recycleview.visibility = View.VISIBLE
//                        invoiceAdapter = InvoiceAdapter(this@SalesInvoiceActivity, invoiceHistoryMainDO.invoiceHistoryDOS)
//                        recycleview.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this@SalesInvoiceActivity)
//
//                        recycleview.adapter = invoiceAdapter
//                    }else{
//                        tvNoDataFound.visibility = View.VISIBLE
//                        recycleview.visibility = View.GONE
//                    }
//
//
//                }
//            }
//            driverListRequest.execute()
//        } else {
//            showAppCompatAlert("Alert!", resources.getString(R.string.internet_connection), getString(R.string.ok), "", "",false)
//
//        }
//
//        val btnCreate = findViewById<Button>(R.id.btnCreate)
//
//        btnCreate.setOnClickListener {
//            Util.preventTwoClick(it)
//            val intent = Intent(this@SalesInvoiceActivity, CreateInvoiceActivity::class.java)
//            intent.putExtra("Sales",fromId)
//            startActivity(intent)
//        }
//    }
//
//}