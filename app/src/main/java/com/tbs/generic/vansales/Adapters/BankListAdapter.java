package com.tbs.generic.vansales.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tbs.generic.vansales.Activitys.CreatePaymentActivity;
import com.tbs.generic.vansales.Activitys.OnAccountCreatePaymentActivity;
import com.tbs.generic.vansales.Model.BankDO;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.utils.PreferenceUtils;

import java.util.ArrayList;

public class BankListAdapter extends RecyclerView.Adapter<BankListAdapter.MyViewHolder>  {

    private ArrayList<BankDO> bankDOS;
    private Context context;
    private int type=1;





    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName;
        private LinearLayout llDetails;

        public MyViewHolder(View view) {
            super(view);
            tvName = view.findViewById(R.id.tvName);



        }
    }


    public BankListAdapter(Context context, ArrayList<BankDO> bankDoS,int typE) {
        this.context = context;
        this.bankDOS = bankDoS;
        this.type    = typE;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.bank_text, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final BankDO bankDO = bankDOS.get(position);

        holder.tvName.setText(""+bankDO.bankName);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = holder.getAdapterPosition();
                PreferenceUtils preferenceUtils = new PreferenceUtils(context);
                preferenceUtils.saveString(PreferenceUtils.BANK_REASON, String.valueOf(bankDOS.get(pos).bankName));
                if(type==1){
                    if(context  instanceof CreatePaymentActivity){
                        ((CreatePaymentActivity)context).tvBankSelection.setText(""+bankDOS.get(pos).bankName);
                        ((CreatePaymentActivity)context).bankDialog.dismiss();
                    }


                }else if(type==2){
                    if(context  instanceof com.tbs.generic.vansales.collector.CreatePaymentActivity){
                        ((com.tbs.generic.vansales.collector.CreatePaymentActivity)context).tvBankSelection.setText(""+bankDOS.get(pos).bankName);
                        ((com.tbs.generic.vansales.collector.CreatePaymentActivity)context).bankDialog.dismiss();
                    }


                }else {
                    if(context  instanceof CreatePaymentActivity){
                        ((CreatePaymentActivity)context).tvBankSelection.setText(""+bankDOS.get(pos).bankName);
                        ((CreatePaymentActivity)context).bankDialog.dismiss();
                    }
                    if(context  instanceof OnAccountCreatePaymentActivity){
                        ((OnAccountCreatePaymentActivity)context).tvBankSelection.setText(""+bankDOS.get(pos).bankName);
                        ((OnAccountCreatePaymentActivity)context).bankDialog.dismiss();
                    }

                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return bankDOS.size();
    }

}
