//package com.tbs.generic.vansales.Activitys
//
//import android.content.Intent
//import android.view.View
//import android.view.ViewGroup
//import android.widget.*
//import com.tbs.generic.vansales.Adapters.PaymentAdapter
//import com.tbs.generic.vansales.Model.LoadStockDO
//import com.tbs.generic.vansales.R
//import com.tbs.generic.vansales.Requests.PaymentHistoryRequest
//import com.tbs.generic.vansales.database.StorageManager
//import com.tbs.generic.vansales.utils.PreferenceUtils
//import com.tbs.generic.vansales.utils.Util
//import java.util.ArrayList
//
//
//class SalesPaymentsActivity : BaseActivity() {
//    lateinit var invoiceAdapter: PaymentAdapter
//    lateinit var loadStockDOs: ArrayList<LoadStockDO>
//    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
//    lateinit var tvNoDataFound: TextView
//    var fromId = 0
//
//    override fun onResume() {
//        super.onResume()
//    }
//
//    override fun initialize() {
//        var llCategories = layoutInflater.inflate(R.layout.framgment_sales_payments_list, null) as RelativeLayout
//        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
//        changeLocale()
//        initializeControls()
//
//        toolbar.setNavigationIcon(R.drawable.back)
//        toolbar.setNavigationOnClickListener { finish() }
//    }
//
//    override fun initializeControls() {
//        tvScreenTitle.setText(R.string.sales_payments)
//        recycleview = findViewById<androidx.recyclerview.widget.RecyclerView>(R.id.recycleview)
//        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
//        tvNoDataFound = findViewById<TextView>(R.id.tvNoDataFound)
//
//        recycleview.layoutManager = linearLayoutManager
//        if (intent.hasExtra("Sales")) {
//            fromId = intent.extras!!.getInt("Sales")
//        }
//        val btnCreate = findViewById<Button>(R.id.btnCreate)
//        if (Util.isNetworkAvailable(this)) {
//
//            val driverListRequest = PaymentHistoryRequest("", this@SalesPaymentsActivity)
//            driverListRequest.setOnResultListener { isError, invoiceHistoryMainDO ->
//                hideLoader()
//                if (isError) {
//                    tvNoDataFound.visibility = View.VISIBLE
//                    recycleview.visibility = View.GONE
//                    Toast.makeText(this@SalesPaymentsActivity, resources.getString(R.string.error_NoData), Toast.LENGTH_SHORT).show()
//                } else {
//
//                    if (invoiceHistoryMainDO.paymentHistoryDOS.size > 0) {
//                        tvNoDataFound.visibility = View.GONE
//                        recycleview.visibility = View.VISIBLE
//                        invoiceAdapter = PaymentAdapter(this@SalesPaymentsActivity, invoiceHistoryMainDO.paymentHistoryDOS)
//                        recycleview.adapter = invoiceAdapter
//                    } else {
//                        tvNoDataFound.visibility = View.VISIBLE
//                        recycleview.visibility = View.GONE
//                    }
//
//
//                }
//            }
//            driverListRequest.execute()
//        } else {
//            showAppCompatAlert(getString(R.string.alert), resources.getString(R.string.internet_connection), getString(R.string.ok), "", "",false)
//
//        }
//
//        btnCreate.setOnClickListener {
//            Util.preventTwoClick(it)
//            val intent = Intent(this@SalesPaymentsActivity, InvoiceListActivity::class.java)
//            intent.putExtra("Sales", fromId)
//            startActivity(intent)
//
//        }
//
//
//    }
//
//
//}